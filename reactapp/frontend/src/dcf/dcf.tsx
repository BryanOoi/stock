import React, { FC } from 'react';
import { useHistory } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import GaugeChart from 'react-gauge-chart';
import Switch from "react-switch";
import { useTable } from 'react-table'
import './Dcf.css';

const useStyles = makeStyles((theme) => ({
  }));

export const Dcf: FC = () => {
    const classes = useStyles();
    const history = useHistory();

    return (
        <div className="container">
            <div className="row company-name">
                <div className="col-12">
                    <h4 className="text-center"> MEGA FIRST CORPORATION </h4> 
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <h6 className="text-center"> Financial Strength </h6> 
                </div>
            </div>
            <div className="row">
                <div className="col-md-6">
                    <h6 className="text-center"> Medium Term </h6> 
                </div>
                <div className="col-md-6">
                    <h6 className="text-center"> Long Term </h6> 
                </div>
            </div>
            <div className="row">
                <div className="col-md-6">
                    <h6 className="text-center"> <GaugeChart id="gauge-chart1" /> </h6> 
                </div>
                <div className="col-md-6">
                    <h6 className="text-center"> <GaugeChart id="gauge-chart1" /> </h6> 
                </div>
            </div>
            <div className="row">
                <div className="col-md-4">
                    <h5 className="text-center"> RM 3.52 price </h5> 
                </div>
                <div className="col-md-4">
                    <h6 className="text-center"> RM 9.28 value </h6> 
                </div>
                <div className="col-md-4">
                    <h6 className="text-center"> 328% upside </h6> 
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <h6 className="text-center"> Change inputs to DCF </h6> 
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <h6 className="text-center"> More Info </h6> 
                    <Switch onChange={() => {}} checked={true} />
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <h6 className="text-center"> Growth Rate </h6> 
                    <input type="text" id="lname" name="lname"></input>
                </div>
            </div>
            <div>
                Table
            </div>
        </div>
    );
};
