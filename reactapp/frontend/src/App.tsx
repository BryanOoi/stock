import React, { FC } from 'react';
import { Routes } from './Routes';
import { Dcf } from './dcf/Dcf';

// const App: FC = () => <Routes />;
const App: FC = () => <Dcf />;

export default App;
