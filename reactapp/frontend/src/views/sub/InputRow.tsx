import React, { FC, useState} from 'react';
import {
    Paper,
    Grid,
    TextField
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import NumberFormat from 'react-number-format'; 

const useStyles = makeStyles((theme) => ({
    margin: {
      margin: theme.spacing(2),
    },
    padding: {
      padding: theme.spacing(1),
    },
    button: {
      textTransform: 'none',
    },
    marginTop: {
      marginTop: 10,
    },
  }));

type AppProps = {
    items: Number[];
};

export const Protected = ({ items }: AppProps) => {
    const classes = useStyles();
    const [revenue, setRevenue] = useState<Number[]>(items);

    // setRevenue({
    //     ...revenue,
    //     [event.target.name]: event.target.value,
    //   });

    const handleChange = (event : React.ChangeEvent<HTMLInputElement>) => {
        let val:Number = parseFloat(event.currentTarget.value);
        if (isNaN(val as any)){
            val = 0;
        }
        setRevenue([val]);
      };

    return (
        <TextField
            id="revenue"
            label="Revenue"
            type="text"
            value={revenue}
            onChange={handleChange}
            InputProps={{ classes: { inputComponent: NumberFormat }  as any }}
            fullWidth
            autoFocus
            required
        />
    );
};
