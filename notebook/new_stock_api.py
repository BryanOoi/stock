import statistics
import requests
from dateutil import parser
from datetime import datetime
import os, shutil, time
import pandas as pd
import stockapi
from io import StringIO
import difflib
import requests_cache
from collections import defaultdict
from mongodb import MetricsDB, StockPriceDB, BondPriceDB, EquityPremiumDB
import stockapi

# requests_cache.install_cache('eod_cache')

API_TOKEN = os.environ.get('API_TOKEN', False)

total_urls = ['http://www.stern.nyu.edu/~adamodar/pc/datasets/ctryprem.xlsx',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/betas.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/betaemerg.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/betaGlobal.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/countrytaxrates.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/totalbeta.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/totalbetaemerg.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/wacc.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/waccemerg.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/ratings.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/capex.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/capexemerg.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/margin.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/marginemerg.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/histgr.xls',
 'http://www.stern.nyu.edu/~adamodar/pc/datasets/histgremerg.xls',
 'http://people.stern.nyu.edu/adamodar/New_Home_Page/datacurrent.html',
 'http://www.stern.nyu.edu/~adamodar/pc/implprem/ERPbymonth.xlsx']

COUNTRY_MAP = {
    'Malaysia' : 'MY',
    'United States' : 'US'
}

# cnx = stockapi.connect_db()
# df_company_profile = stockapi.read_data(cnx, table_name="company_profile")
# df_company_profile.drop_duplicates(subset ="stockcode", inplace = True, keep="last") 
# df_company_profile = df_company_profile.sort_values("pt_difference", ascending=False)


# stock_generals = []
# for file in os.listdir('eod_data/klse_stock_fundamentals'):
#     stock_data = stockapi.read_json(f'eod_data/klse_stock_fundamentals/{file}')
#     stock_generals.append(stock_data['General'])

# code_data = {}
# no_company_found = []
# reuters_company_name_list = list(df_company_profile['company_name'])

# for temp in stock_generals:
#     company_name = temp['Name']
#     company_code = f"{temp['Code']}.{temp['Exchange']}"
#     limited_df = df_company_profile[df_company_profile['company_name'] == company_name]
    
#     if len(limited_df) == 0:
#         no_company_found.append(company_name)
#         closest_company_match = difflib.get_close_matches(company_name, reuters_company_name_list, n=1, cutoff=0)[0]
#         limited_df = df_company_profile[df_company_profile['company_name'] == closest_company_match]
        
#     company_sector = list(limited_df['sector'])
#     company_industry = list(limited_df['industry'])
#     code_data[company_code] = {
#         'sector': company_sector,
#         'industry': company_industry,
#         'name': company_name
#     }

code_data = stockapi.read_json('/root/stock/stock/notebook/eod_data/klse_stock_fundamentals/klse_industry.json.gz')

def update_bulk_stock_price(API_TOKEN, country):
    stockprice_db = StockPriceDB()
    url = f'https://eodhistoricaldata.com/api/eod-bulk-last-day/{country}?api_token={API_TOKEN}&fmt=json'
    x = requests.get(url)
    json_data = x.json()
#     price = json_data['close']
#     if price == 'NA':
#         price = json_data['previousClose']
    stockprice_db.insert(json_data)
    return True
    
def update_bond_price(API_TOKEN):
    bondprice_db = BondPriceDB()
    bondprice_data = []
    for country_code in COUNTRY_MAP.values():
        country_bond = f'{country_code}10Y.GBOND'
        x = requests.get(f'https://eodhistoricaldata.com/api/eod/{country_bond}?api_token={API_TOKEN}&fmt=json')
        bondprice_data.append({
            'country': country_code,
            'price': float(x.json()[-1]['close'])/100,
            'date': datetime.today().isoformat()
        })
    bondprice_db.insert(bondprice_data)
    return True
    
def print_d(text, debug=True):
    if debug:
        print(text)
        
def get_stock_eod(API_TOKEN, stock_code):
#     url = 'https://eodhistoricaldata.com/api/fundamentals/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX'
    url = f'https://eodhistoricaldata.com/api/fundamentals/{stock_code}?api_token={API_TOKEN}'
    x = requests.get(url)
    json_data = x.json()
    
    return json_data

def read_latest_date():
    with open("/root/stock/stock/notebook/aswath_data/last_date.txt", "r") as f:
        for line in f:
            return line
        
def write_latest_date(date):
    with open("/root/stock/stock/notebook/aswath_data/last_date.txt", "w") as f:
        f.write(date)
    
def get_latest_website_date():
    latest_website = 'http://people.stern.nyu.edu/adamodar/New_Home_Page/datacurrent.html'

    x = requests.get(latest_website)
    latest_website_date = x.text.split('Data of last update:')[1].split('</strong></em>')[0].split(' <em><strong>')[1]
    return latest_website_date
        
def refresh_aswath_excel_data(latest_website_date, total_urls):
    last_date = read_latest_date()

    BASE_PATH = '/root/stock/stock/notebook/aswath_data/excels'
    if latest_website_date != last_date:
        try:
            shutil.rmtree(BASE_PATH)
        except Exception as e:
            print(e)
        os.mkdir(BASE_PATH)
        for url in total_urls:
            resp = requests.get(url)
            xls_file_name = url.split('/')[-1]
            output = open(BASE_PATH + '/' + xls_file_name, 'wb')
            output.write(resp.content)
            output.close()
            time.sleep(10)
        write_latest_date(latest_website_date)
        
def get_all_excels():
    dfs_beta_emerg = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/betaemerg.xls', sheet_name='Industry Averages', header=9)
    dfs_beta_global = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/betaGlobal.xls', sheet_name='Industry Averages', header=9)
    dfs_beta_us = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/betas.xls', sheet_name='Industry Averages', header=9)
    dfs_capex_us = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/capex.xls', sheet_name='Industry Averages', header=7)
    dfs_capex_emerg = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/capexemerg.xls', sheet_name='Industry Averages', header=7)
    dfs_marginal_tax = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/countrytaxrates.xls', sheet_name='Sheet1', header=0)
    dfs_risk_premium = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/ctryprem.xlsx', sheet_name='Regional breakdown', header=0, engine='openpyxl')
    dfs_bond_pct = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/ctryprem.xlsx', sheet_name='Equity vs Govt Bond vol', header=2, engine='openpyxl')
    dfs_histgr_us = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/histgr.xls', sheet_name='Sheet1', header=7)
    dfs_histgr_emerg = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/histgremerg.xls', sheet_name='Sheet1', header=7)
    dfs_margin_us = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/margin.xls', sheet_name='Sheet1', header=8)
    dfs_margin_emerg = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/marginemerg.xls', sheet_name='Sheet1', header=8)
    dfs_ratings_large = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/ratings.xls', sheet_name='Start here Ratings sheet', header=17, usecols="A:D", nrows=15)
    dfs_ratings_small = pd.read_excel('/root/stock/stock/notebook/aswath_data/excels/ratings.xls', sheet_name='Start here Ratings sheet', header=36, usecols="A:D", nrows=15)
    
    return (dfs_beta_emerg,
            dfs_beta_global,
            dfs_beta_us,
            dfs_capex_us,
            dfs_capex_emerg,
            dfs_marginal_tax,
            dfs_risk_premium,
            dfs_bond_pct,
            dfs_histgr_us,
            dfs_margin_us,
            dfs_margin_emerg,
            dfs_ratings_large,
            dfs_ratings_small
           )

(dfs_beta_emerg,
            dfs_beta_global,
            dfs_beta_us,
            dfs_capex_us,
            dfs_capex_emerg,
            dfs_marginal_tax,
            dfs_risk_premium,
            dfs_bond_pct,
            dfs_histgr_us,
            dfs_margin_us,
            dfs_margin_emerg,
            dfs_ratings_large,
            dfs_ratings_small
           ) = get_all_excels()



def get_latest_stock_price(stock_code):
#     url = f'https://eodhistoricaldata.com/api/real-time/{stock_code}?api_token={API_TOKEN}&fmt=json'
#     x = requests.get(url)
#     json_data = x.json()
#     price = json_data['close']
#     if price == 'NA':
#         price = json_data['previousClose']
    try:
        stock_code_split = stock_code.split('.')
        a = StockPriceDB()
        price = a.query({'code': stock_code_split[0]})[0]['close']
    except:
        print(f'Cannot find stock price for stock: {stock_code}')
        price = 99999
    return price

def map_industry():
    industry_map = {}

    list_of_industry = list(set(df_company_profile['industry']))

    convert_from = sorted(list_of_industry)
    convert_to = list(dfs_beta_emerg['Industry Name'])

    for industry in convert_from:
        closest_industry_match = difflib.get_close_matches(industry, convert_to, n=1, cutoff=0)[0]
        industry_map[industry] = closest_industry_match
        
def get_bond_yield_latest(country):

    country_bond = f'{COUNTRY_MAP[country]}10Y.GBOND'
    
    a = BondPriceDB()
    return a.query({'country': COUNTRY_MAP[country]})[0]['price']

def get_sector_convertor(gic_industry):
    #TODO ADD MORE OF THIS
    industry_map = stockapi.read_json('/root/stock/stock/notebook/eod_data/industry_map.json')
    return industry_map[gic_industry]


def get_latest_equity_risk_premium():
    home_website = 'http://people.stern.nyu.edu/adamodar/New_Home_Page/home.htm'

    x = requests.get(home_website)
    equity_risk_premium = x.text.split('</strong>= ')[1].split('%')[0].strip()
    equity_risk_premium = float(equity_risk_premium)/100
    
    return equity_risk_premium

def get_latest_equity_risk_premium_cached():
    equity_premium = EquityPremiumDB()
    return equity_premium.query({})[0]['premium']

def get_lease_changes(operating_lease, cost_of_debt, ebit, interest_expenses):
    #UNFORTUNATELY, WE CAN ONLY GET LEASE VALUES FOR LATEST YEAR, SO WE HAVE TO DISCOUNT IT FOR 5 YEARS LIKE AN ANNUITY.
    YEARS_ANNUITY = 5
    
    debt_value_of_leases = f"{operating_lease} * (1 - (1 + {cost_of_debt}) ** ({- YEARS_ANNUITY})) / {cost_of_debt}"
#     debt_value_of_leases = f"{operating_lease} * ((1 + {cost_of_debt}) ** { YEARS_ANNUITY})"
    depreciation_lease = f"{ debt_value_of_leases } / ( {YEARS_ANNUITY})"
    
    adjustment_to_operating_earnings_str = f"{operating_lease} - {depreciation_lease}"
    
    return {
        'depreciation_lease': eval(depreciation_lease),
        'debt_value_of_leases': eval(debt_value_of_leases),
        'adjustment_to_operating_earnings': eval(adjustment_to_operating_earnings_str)
    }

def get_cost_of_debt(market_cap, 
                     risk_free_rate, 
                     default_spread_country,
                     operating_lease, 
                     interest_expenses, 
                     ebit,
                     country,
                     debug = False):
    
    USD_TO_MYR = 4
    SMALL_CAP_THRESHOLD = 5000000000
    
    df_rating = None
    dfs_beta = None
    
    if country == 'Malaysia':
        market_cap *= USD_TO_MYR
        
    if market_cap > SMALL_CAP_THRESHOLD:
        df_rating = dfs_ratings_small
    else:
        df_rating = dfs_ratings_large
    
    i = 0
    
    change_lease = None
    while True:
        if interest_expenses < 1:
            interest_coverage_ratio = 99999
        else:
            interest_coverage_ratio = ebit/interest_expenses

        company_default_spread = list(df_rating[df_rating[df_rating.columns[1]] > interest_coverage_ratio][df_rating.columns[3]])[0]

        cost_of_debt_str = f"{risk_free_rate} + {company_default_spread} + {default_spread_country}"

        cost_of_debt = eval(cost_of_debt_str)
    
        if not operating_lease:
            break
        
        change_lease = get_lease_changes(operating_lease, cost_of_debt, ebit, interest_expenses)
        
        ebit += change_lease['adjustment_to_operating_earnings']
        
        i+=1
        
        if i == 50:
            break
    if debug:
        print('Cost of debt : ',f"{cost_of_debt_str} = {cost_of_debt}" )
        print(change_lease) 
    return {
        "cost_of_debt" : cost_of_debt,
        "change_lease": change_lease,
        "interest_coverage_ratio": interest_coverage_ratio,
        "cost_of_debt_calc" : f"{cost_of_debt_str} = {cost_of_debt}"
    }

def get_cost_of_capital(country, 
                        sector, 
                        total_equity, 
                        total_debt, 
                        ebit, 
                        interest_expense, 
                        market_cap, 
                        bond_yield,
                        equity_risk_premium,
                        operating_lease = 0,
                        pct_of_revenues = 1,
                        debug = False):
    #ASSUMPION IS THAT ALL REVENUES COMES FROM THE COUNTRY, THIS IS OBVIOUSLY WRONG. 
    #TODO: FIGURE OUT HOW TO GET pct_of_revenues PROGRAMMITICALLY
    
    BOND_DISCOUNT_YEARS = 3
    CLAMP_BETA_MIN = 0.8
    CLAMP_BETA_MAX = 2.0
    
    df_rating = None
    dfs_beta = None
        
    if country == 'United States':
        dfs_beta = dfs_beta_us
    else:
        dfs_beta = dfs_beta_emerg
        
    
    unlevered_beta = list(dfs_beta[dfs_beta['Industry Name'] == sector]['Unlevered beta corrected for cash'])[0]
    
    df_risk_premium_country = dfs_risk_premium[dfs_risk_premium['Country'] == country]
    
    country_risk_premium = list(df_risk_premium_country['Country Risk Premium'])[0]
    
    marginal_tax_rate = list(df_risk_premium_country['Corporate Tax Rate'])[0]
    
    default_spread_country = list(df_risk_premium_country['Adj. Default Spread'])[0]
    
    try:
        stock_market_volatility = list(dfs_bond_pct[dfs_bond_pct['Country'] == country]['sEquity/ sBond'])[0]
    except:
        stock_market_volatility = 0
    
    market_value_of_equity = market_cap
    
    risk_free_rate_str = f"{bond_yield} - {default_spread_country}"
    
    risk_free_rate = eval(risk_free_rate_str)
    
    debt_data = get_cost_of_debt(market_cap,
                         bond_yield, 
                         default_spread_country,
                         operating_lease, 
                         interest_expense, 
                         ebit,
                         country,
                         debug = debug)
    
#     cost_of_debt_str = f"{risk_free_rate} + {company_default_spread} + {default_spread_country}"
    
#     cost_of_debt = eval(cost_of_debt_str)
        
    cost_of_debt = debt_data['cost_of_debt']
    
    if debug:
        print("Debt before: ", total_debt)
        print("Ebit before: ", ebit)
    
    if debt_data['change_lease']:
        total_debt += debt_data['change_lease']['debt_value_of_leases']
        ebit += debt_data['change_lease']['adjustment_to_operating_earnings']
        
    if debug:
        print("Debt after: ", total_debt)
        print("Ebit after: ", ebit)
    
    market_value_of_debt_str = f"{interest_expense} * (1 - (1 + {cost_of_debt}) ** ( - {BOND_DISCOUNT_YEARS})) / {cost_of_debt} + {total_debt} / (1 + {cost_of_debt}) ** {BOND_DISCOUNT_YEARS}"
    
    market_value_of_debt = eval(market_value_of_debt_str)

    debt_to_equity_ratio = market_value_of_debt / market_cap
    
    equity_risk_premium = equity_risk_premium
    
    relevered_beta_str = f"({unlevered_beta}) * (1 + (1 - {marginal_tax_rate}) * ({debt_to_equity_ratio}))"
    
    relevered_beta = eval(relevered_beta_str)
    
    if debug:
        print("Relevered beta : ", f"{relevered_beta_str} = {relevered_beta}")
    
    adjusted_relevered_beta = sorted((CLAMP_BETA_MIN, relevered_beta, CLAMP_BETA_MAX))[1]
    
    if debug:    
        print("Risk free rate : ", f"{risk_free_rate_str} = {risk_free_rate}")
    
    cost_of_equity_str = f"{risk_free_rate} + {adjusted_relevered_beta} * ({equity_risk_premium}) + {pct_of_revenues} * {country_risk_premium}"
    
    cost_of_equity = eval(cost_of_equity_str)
    
    after_tax_cost_of_debt_str = f"{cost_of_debt} * (1 - {marginal_tax_rate})"
    
    after_tax_cost_of_debt = eval(after_tax_cost_of_debt_str)
    
    market_value_of_equity = market_cap
    
    if debug:    
        print("Market Value of debt : ", f"{market_value_of_debt_str} = {market_value_of_debt}")
    
    pct_equity = market_value_of_equity / (market_value_of_equity + market_value_of_debt)
    
    pct_debt = market_value_of_debt / (market_value_of_equity + market_value_of_debt)
    
    wacc_str = f"{cost_of_equity} * {pct_equity} + {after_tax_cost_of_debt} * {pct_debt}"
    
    wacc = eval(wacc_str)
    
    if debug:    
        print('Cost of equity : ' , "risk_free_rate + adjusted_relevered_beta * (equity_risk_premium) + pct_of_revenues * country_risk_premium")
        print('Cost of equity : ', f"{cost_of_equity_str} = {cost_of_equity}")
        print("After tax cost of debt : ", f"{after_tax_cost_of_debt_str} = {after_tax_cost_of_debt}")
        print("WACC : ", f"{wacc_str} = {wacc}")
        
    calculations = {
        'cost_of_equity': f"{cost_of_equity_str} = {cost_of_equity}",
        'after_tax_cost_of_debt': f"{after_tax_cost_of_debt_str} = {after_tax_cost_of_debt}",
        'wacc': f"{wacc_str} = {wacc}",
        'market_value_of_debt': f"{market_value_of_debt_str} = {market_value_of_debt}",
        'risk_free_rate' : f"{risk_free_rate_str} = {risk_free_rate}",
        'relevered_beta' : f"{relevered_beta_str} = {relevered_beta}",
        'cost_of_debt' : debt_data['cost_of_debt_calc']
    }
    return wacc, debt_data['change_lease'], risk_free_rate, debt_to_equity_ratio, debt_data['interest_coverage_ratio'], calculations


def floatify(num):
    if not num:
        return 0
    else:
        return float(num)

def get_marginal_tax_rate(json_data):
    try:
        stock_country = json_data['General']['AddressData']['Country']
    except:
        stock_country = json_data['General']['CountryName']
    df_risk_premium_country = dfs_risk_premium[dfs_risk_premium['Country'] == stock_country]
    marginal_tax_rate = list(df_risk_premium_country['Corporate Tax Rate'])[0]
    return marginal_tax_rate

def convert_data_to_data_needed(json_data, debug = False):
    current_stock = json_data
    
    try:
        country = json_data['General']['AddressData']['Country']
    except:
        country = json_data['General']['CountryName']
    industry = {
        'Sector':current_stock['General']['Sector'],
        'Industry':current_stock['General']['Industry'],
        'GicSector':current_stock['General']['GicSector'],
        'GicGroup':current_stock['General']['GicGroup'],
        'GicIndustry':current_stock['General']['GicIndustry'],
        'GicSubIndustry': current_stock['General']['GicSubIndustry']
    }
    
    stock_exchange = json_data['General']['Exchange']
    if stock_exchange == 'NASDAQ' or stock_exchange == 'NYSE':
        stock_exchange = 'US'
    
    stock_code = f"{json_data['General']['Code']}.{stock_exchange}"
    
    last_year_date = list(current_stock['Financials']['Income_Statement']['yearly'].keys())[0]
    latest_quarter_date = list(current_stock['Financials']['Balance_Sheet']['quarterly'].keys())[0]

    ttm_data = get_last_ttm(current_stock, 
                            ['operatingIncome', 
                             'interestExpense', 
                             'totalStockholderEquity', 
                             'shortLongTermDebtTotal', 
                             'capitalLeaseObligations',
                            'cashAndShortTermInvestments',
                            'minorityInterest',
                            'totalRevenue',
                            'interestIncome'])

    ttm_data = ttm_data['year 1']
    revenues_ttm = ttm_data['totalRevenue']
    revenues_last_year = current_stock['Financials']['Income_Statement']['yearly'][last_year_date]['totalRevenue']
    
    operating_income_ttm = ttm_data['operatingIncome']
    operating_income_last_year = current_stock['Financials']['Income_Statement']['yearly'][last_year_date]['operatingIncome']

    interest_income = ttm_data['interestIncome']
    if interest_income < 0:
        if debug:
            print('WARNING: USING -ve INTEREST INCOME INSTEAD OF INTEREST EXPENSE')
        interest_expense_ttm = ttm_data['interestIncome']
        interest_expense_last_year = current_stock['Financials']['Income_Statement']['yearly'][last_year_date]['interestIncome']
        interest_expense_ttm = - floatify(interest_expense_ttm)
        interest_expense_last_year = - floatify(interest_expense_last_year)
    else:
        interest_expense_ttm = ttm_data['interestExpense']
        interest_expense_last_year = current_stock['Financials']['Income_Statement']['yearly'][last_year_date]['interestExpense']
        interest_expense_ttm = floatify(interest_expense_ttm)
        interest_expense_last_year = floatify(interest_expense_last_year)      
        
    interest_expense_ttm = abs(interest_expense_ttm)
    interest_expense_last_year = abs(interest_expense_last_year)

    book_value_of_equity_ttm = ttm_data['totalStockholderEquity']
    book_value_of_equity_last_year = current_stock['Financials']['Balance_Sheet']['yearly'][last_year_date]['totalStockholderEquity']

    book_value_of_debt_ttm = ttm_data['shortLongTermDebtTotal']
    book_value_of_debt_last_year = current_stock['Financials']['Balance_Sheet']['yearly'][last_year_date]['shortLongTermDebtTotal']

    capital_lease = current_stock['Financials']['Balance_Sheet']['yearly'][last_year_date]['capitalLeaseObligations']
    research_development = current_stock['Financials']['Income_Statement']['yearly'][last_year_date]['researchDevelopment']

    cash_ttm =  ttm_data['cashAndShortTermInvestments']
    cash_last_year = current_stock['Financials']['Balance_Sheet']['yearly'][last_year_date]['cashAndShortTermInvestments']

    minority_interest_ttm = ttm_data['minorityInterest']
    minority_interest_last_year = current_stock['Financials']['Income_Statement']['yearly'][last_year_date]['minorityInterest']

    stock_price = get_latest_stock_price(stock_code)
    
    shares_outstanding = floatify(current_stock['SharesStats']['SharesOutstanding'])
    
    if -0.1 < shares_outstanding < 0.1:
        shares_outstanding = floatify(current_stock['Financials']['Balance_Sheet']['quarterly'][latest_quarter_date]['commonStockSharesOutstanding'])
    if -0.1 < shares_outstanding < 0.1:
        if stock_price != 99999 and floatify(current_stock['Highlights']['MarketCapitalization']) > 1:
            shares_outstanding = floatify(current_stock['Highlights']['MarketCapitalization']) / floatify(stock_price)
    if -0.1 < shares_outstanding < 0.1:
        shares_outstanding = 99999999999
            
#     tax_expense = floatify(current_stock['Financials']['Income_Statement']['yearly'][last_year_date]['incomeTaxExpense'])
#     tax_provision = floatify(current_stock['Financials']['Income_Statement']['yearly'][last_year_date]['taxProvision'])

    tax_expense = floatify(current_stock['Financials']['Income_Statement']['yearly'][last_year_date]['taxProvision'])
    
    marginal_tax_rate = floatify(get_marginal_tax_rate(json_data))
    
    try:
        effective_tax_rate = (tax_expense
                              /(floatify(current_stock['Financials']['Income_Statement']['yearly'][last_year_date]['operatingIncome']) 
                               + (floatify(interest_expense_last_year) )))
    except:
        effective_tax_rate = marginal_tax_rate
    
    
    if effective_tax_rate > marginal_tax_rate:
        effective_tax_rate = marginal_tax_rate
        

        
    market_cap = floatify(current_stock['Highlights']['MarketCapitalization'])
    if market_cap < 1:
        #Get estimated market cap
        market_cap = float(shares_outstanding) * stock_price
        
    
    reports_time_diff = (parser.parse(latest_quarter_date) - parser.parse(last_year_date)).days/365
    
    return (
        industry,
        float(revenues_ttm if revenues_ttm else 0),
        float(revenues_last_year if revenues_last_year else 0),
        float(operating_income_ttm if operating_income_ttm else 0),
        float(operating_income_last_year if operating_income_last_year else 0),
        float(interest_expense_ttm if interest_expense_ttm else 0),
        float(interest_expense_last_year if interest_expense_last_year else 0),
        float(book_value_of_equity_ttm if book_value_of_equity_ttm else 0),
        float(book_value_of_equity_last_year if book_value_of_equity_last_year else 0),
        float(book_value_of_debt_ttm if book_value_of_debt_ttm else 0),
        float(book_value_of_debt_last_year if book_value_of_debt_last_year else 0),
        float(capital_lease if capital_lease else 0),
        float(research_development if research_development else 0),
        float(cash_ttm if cash_ttm else 0),
        float(cash_last_year if cash_last_year else 0),
        float(minority_interest_ttm if minority_interest_ttm else 0),
        float(minority_interest_last_year if minority_interest_last_year else 0),
        float(shares_outstanding if shares_outstanding else 0),
        float(effective_tax_rate if effective_tax_rate else 0),
        float(marginal_tax_rate if marginal_tax_rate else 0),
        float(market_cap if market_cap else 0),
        float(stock_price if stock_price else 0),
        reports_time_diff
    )

######################################
############# HELPERS ################
######################################

"""
The pct below basically it makes two values not too different from one another. 
For example if margins is 30% while difference in margin is 20% or more, the ratio between the two will be 0.66 which is 
higher than the pct threshold. It limits the new margin to be 45% instead of 50%
"""

CLAMP_DIFFERENCE_PCT = 0.5

def clamp_difference(main_value, difference):
    if main_value > 0:
        if abs(difference / main_value) > CLAMP_DIFFERENCE_PCT:
            new_difference = (main_value * CLAMP_DIFFERENCE_PCT) if difference > 0 else - (main_value * CLAMP_DIFFERENCE_PCT)
            return main_value - new_difference
        else:
            return main_value - difference
    else:
        return 0

def get_last_ttm(stock_data, metrics, years=1):
    assert isinstance(metrics, list)
    QUARTERS_IN_YEARS = 4
    final_data = defaultdict(dict)
    stock_data_financials = stock_data['Financials']
    stock_data_financials_keys = list(stock_data_financials.keys())

    start_index = 0
    end_index = QUARTERS_IN_YEARS
    
    for metric in metrics:
        correct_location = None
        if '::' in metric:
            split_metric = metric.split('::')
            correct_location = stock_data_financials[split_metric[1]]['quarterly']
            metric = split_metric[0]
            key = split_metric[1]
        else:
            for key in stock_data_financials_keys:
                first_date = list(stock_data_financials[key]['quarterly'].keys())[0]
                if metric in stock_data_financials[key]['quarterly'][first_date]:
                    correct_location = stock_data_financials[key]['quarterly']
                    break
        assert correct_location != None, f'Metric {metric} cannot be found in financials'
        
        for i in range(years):
            last_four_quarters = list(correct_location.keys())[start_index:end_index]
            total_metric_value = 0
            if key == "Balance_Sheet" or key == "Cash_Flow":
                value = correct_location[last_four_quarters[0]][metric]
                value = 0 if not value else value

                total_metric_value = float(value)
            else:
                for quarter in last_four_quarters:
                    value = correct_location[quarter][metric]
                    value = 0 if not value else value
                    total_metric_value += float(value)
            final_data[f'year {i+1}'][metric] = total_metric_value
            start_index += QUARTERS_IN_YEARS
            end_index += QUARTERS_IN_YEARS
        start_index = 0
        end_index = QUARTERS_IN_YEARS
    return final_data

def get_comparison_items(stock_data, metrics, max_years = 10):
    assert isinstance(metrics, list)
    ttm = get_last_ttm(stock_data, metrics)['year 1']
    
    final_data = {}
    stock_data_financials = stock_data['Financials']
    stock_data_financials_keys = list(stock_data_financials.keys())
    for metric in metrics:
        correct_location = None
        if '::' in metric:
            split_metric = metric.split('::')
            correct_location = stock_data_financials[split_metric[1]]['yearly']
            metric = split_metric[0]
            key = split_metric[1]
        else:
            for key in stock_data_financials_keys:
                first_date = list(stock_data_financials[key]['yearly'].keys())[0]
                if metric in stock_data_financials[key]['yearly'][first_date]:
                    correct_location = stock_data_financials[key]['yearly']
                    break
        assert correct_location != None, f'Metric {metric} cannot be found in financials'
        max_years = min(len(list(correct_location.keys())), max_years)
        last_ten_years = list(correct_location.keys())[:max_years]

        total_data = {}
        total_data['ttm'] = ttm[metric]
        for year in last_ten_years:
            value = correct_location[year][metric]
            value = 0 if not value else value
            total_data[year] = float(value)
         
        final_data[metric] = total_data    
    return final_data

def next_rate(i, decay, ori_growth_rate, perpetual_growth_rate):
    if i <= decay:
        return ori_growth_rate
    else:
        next_current = ori_growth_rate - ((ori_growth_rate - perpetual_growth_rate)/decay) * (i - decay)
    return next_current

def next_tax(i, decay, temp_tax_rate, effective_tax_rate, marginal_tax_rate):
    if i <= decay:
        return effective_tax_rate
    else:
        next_current = temp_tax_rate + (marginal_tax_rate - effective_tax_rate)/decay
    return next_current   

def next_discount(i, decay, temp_discount, original_discount, final_discount):
    if i <= decay:
        return original_discount
    else:
        next_current = temp_discount - (original_discount - final_discount)/decay
    return next_current   

def next_margin(i, margin_convergence, estimated_margins, terminal_estimated_margin):
    if i > margin_convergence:
        return terminal_estimated_margin  
    elif i == 1:
        return estimated_margins
    else:
        next_current = terminal_estimated_margin-(( terminal_estimated_margin - estimated_margins ) / margin_convergence) * ( margin_convergence - i)
    return next_current

def get_list_differences(metric_list, total_years = 10):
    #Not counting TTM data
    
    assert isinstance(metric_list, list)
    
    final_data = {}
    metric_list = metric_list[1:total_years+2]
    if len(metric_list) > 2:
        metric_growth = []
        for i in range(len(metric_list)-1):
            if abs(metric_list[i]) < 0.01 or abs(metric_list[i+1]) < 0.01:
                break
            metric_growth.append((metric_list[i] - metric_list[i+1])/metric_list[i])
        
        cagr = get_cagr(metric_list, years=total_years)

        final_data = {
            'metric_growth' : metric_growth,
            'cagr': cagr,
            'stdev': statistics.stdev(metric_list) if len(metric_growth) > 1 else 0.0001,
            'mean': statistics.mean(metric_list) if len(metric_growth) > 1 else 0.0001,
            'percent_winning': len([x for x in metric_growth if x > 0])/len(metric_growth) if len(metric_growth) > 0 else 0.0001
        }
            
    return final_data

def get_yearly_differences(yearly_items, metrics, total_years = 10):
    #Not counting TTM data
    
    assert isinstance(metrics, list)
    
    final_data = {}
    for metric in metrics:
        metric_list = list(yearly_items[metric].values())[1:total_years+2]
        if len(metric_list) > 2:
            metric_growth = []
            for i in range(len(metric_list)-1):
                if metric_list[i] < 0.01 or metric_list[i+1] < 0.01:
                    break
                metric_growth.append((metric_list[i] - metric_list[i+1])/metric_list[i])
            if metric_list[0] > 0 and metric_list[-1] < 0:
                cagr = 99999
            elif metric_list[0] < 0 and metric_list[-1] < 0:
                cagr = 0
            elif metric_list[0] < 0 and metric_list[-1] > 0:
                cagr = -99999
            else:
                try:
                    cagr = (metric_list[0]/metric_list[-1])**(1/(len(metric_list))) - 1
                except:
                    cagr = 0

            final_data[metric] = {
                'metric_growth' : metric_growth,
                'cagr': cagr,
                'stdev_growth': statistics.stdev(metric_growth) if len(metric_growth) > 1 else 0.0001,
                'mean_growth': statistics.mean(metric_growth) if len(metric_growth) > 1 else 0.0001,
                'percent_winning': len([x for x in metric_growth if x > 0])/len(metric_growth) if len(metric_growth) > 0 else 0.0001
            }
            if isinstance(cagr, complex):
                print(metric_list)
        
    return final_data

def research_development_conversion(research_development_list, sector):
    # NOT INCLUDING TTM
    if not research_development_list[0]:
        return None
    
    sector_to_period = {}
    with open("/root/stock/stock/notebook/aswath_data/r&d.txt", "r") as f:
        for line in f:
            line_split = line.split()
            sector_to_period[' '.join(line_split[:-1])] = line_split[-1]
            
    research_development = research_development_list
    
    actual_amortization_years = int(sector_to_period[sector])

    if actual_amortization_years > len(research_development_list) - 1:
        print(f"Not enough R&D data found, will amortize for {len(research_development_list)  - 1} years.")
        amortization_years = len(research_development_list)  - 1
    else:
        print(f"Amortization of R&D period:  {actual_amortization_years} years.")
        amortization_years = actual_amortization_years
    
    list_of_amortization_years = [amortization_years]
    
    for amortization_years in list_of_amortization_years:
        sum_of_unamortized = 0
        sum_of_amortization = 0
        for i in range(amortization_years + 1):
            try:
                pct_amortization = (amortization_years + ( - i ) ) / amortization_years
                unamortized_portion = pct_amortization * research_development[i]
                amortization_this_year = research_development[i] / amortization_years

                sum_of_unamortized += unamortized_portion
                if i != 0:
                    sum_of_amortization += amortization_this_year
            except:
                pass
                
        adjustment_to_operating_income = research_development_list[0] - sum_of_amortization
    
    return {
        'adjustment_to_operating_income': adjustment_to_operating_income,
        'sum_of_unamortized': sum_of_unamortized,
        'sum_of_amortization': sum_of_amortization
    }
    
def get_yearly_reinvestment(yearly_items, debug=False):
    must_items = ['cashAndShortTermInvestments', 
                  'totalCurrentAssets', 
                  'totalCurrentLiabilities',
                  'changeInWorkingCapital',
                  'capitalExpenditures',
                  'depreciationAndAmortization',
                  'reconciledDepreciation',
                  'capitalExpenditures',
                 'operatingIncome',
                  'totalRevenue',
                 'totalStockholderEquity',
                 'shortLongTermDebtTotal',
                  'taxProvision',
                 'incomeTaxExpense',
                 'researchDevelopment',
                 'intangibleAssets',
                 'goodWill',
                 'totalAssets',
                'totalLiab']
    for item in must_items:
        assert item in yearly_items, item
        
    depreciation_amortization = list(yearly_items['depreciationAndAmortization'].values())

#     if sum(depreciation_amortization) < 1:
#         depreciation_amortization = list(yearly_items['reconciledDepreciation'].values())
#         if depreciation_amortization[0] < 1 and depreciation_amortization[1] > 1:
#             depreciation_amortization[0] = depreciation_amortization[1]
            
    income_tax_expense = list(yearly_items['incomeTaxExpense'].values())
    if sum(income_tax_expense[:3]) < 1:
        income_tax_expense = list(yearly_items['taxProvision'].values())
    
    
    adjusted_ebit = [x - y for x, y in zip(list(yearly_items['operatingIncome'].values()),
                                           income_tax_expense)]
    
    invested_capital = [x + y - z for x, y, z in zip(list(yearly_items['totalStockholderEquity'].values()),
                                                     list(yearly_items['shortLongTermDebtTotal'].values()),
                                                     list(yearly_items['cashAndShortTermInvestments'].values())) ]
    
    net_working_capital =  [x - y - z for x, y, z in zip(list(yearly_items['totalCurrentAssets'].values()),
                            list(yearly_items['totalCurrentLiabilities'].values()),
                             list(yearly_items['cash'].values()))]
    
    change_in_working_capital = [net_working_capital[i] - net_working_capital[i+1] for i in range(len(net_working_capital) - 1)]
    
    
    reinvestment = [w - x + y + z for  w, x, y, z in zip(list(yearly_items['capitalExpenditures'].values())[:-1],
                                                     depreciation_amortization[:-1],
                                                    list(yearly_items['researchDevelopment'].values())[:-1],
                                                     change_in_working_capital)]
    
    reinvestment_2 = [invested_capital[i] - invested_capital[i+1] for i in range(len(invested_capital)-1)]    
    
    book_value =  [x - y - z for x, y, z in zip(list(yearly_items['totalAssets'].values()),
                            list(yearly_items['totalLiab'].values()),
                             list(yearly_items['goodWill'].values()))]
    
    index = len(invested_capital)
    for i, item in enumerate(invested_capital):
        if item < 1 and item > -0.1:
            index = i
            break
            
            
    reinvestment_rate = [x / y if y > 0 else 0.0001 for x, y in zip(list(reinvestment)[:index],
                                           list(adjusted_ebit)[:index])]
    
    reinvestment_rate_2 = [x / y if y > 0 else 0.0001 for x, y in zip(list(reinvestment_2),
                                           list(adjusted_ebit))]
    
    return_on_capital = [x / y if y > 0 else 0.0001 for x, y in zip(list(adjusted_ebit)[:index],
                                           list(invested_capital)[:index])]
    
    operating_margin = [x / y if y > 0 else 0.0001 for x, y in zip(list(yearly_items['operatingIncome'].values()),
                                           list(yearly_items['totalRevenue'].values()))]
    
    sales_to_capital = [x / y if y > 0 else 0.0001 for x, y in zip(list(yearly_items['totalRevenue'].values())[:index],
                                           invested_capital[:index])]
    
    return {
        'adjusted_ebit': adjusted_ebit,
        'invested_capital': invested_capital,
        'reinvestment': reinvestment,
        'reinvestment_rate': reinvestment_rate,
        'reinvestment_rate_2': reinvestment_rate_2,
        'return_on_capital': return_on_capital,
        'operating_margin': operating_margin,
        'sales_to_capital': sales_to_capital,
        'est_growth_rate': [x * y if x > 0 and y > 0 else 0 for x, y in zip(reinvestment_rate,
                                           return_on_capital[:-1]) ],
        'est_growth_rate_2': [x * y if x > 0 and y > 0 else 0 for x, y in zip(reinvestment_rate_2,
                                           return_on_capital[:-1]) ],     
        'book_value': book_value
    }
    
def get_corrected_lease_and_research(research_correction, lease_correction, ebit, invested_capital, total_debt):
    if not research_correction:
        research_correction = {
            'adjustment_to_operating_income': 0,
            'sum_of_unamortized': 0,
            'sum_of_amortization': 0
        }
    
    if not lease_correction:
        lease_correction = {
            'depreciation_lease': 0, 
            'debt_value_of_leases': 0, 
            'adjustment_to_operating_earnings': 0
        }
    
    invested_capital_correction = research_correction['sum_of_unamortized'] + lease_correction['debt_value_of_leases']
    ebit_correction = lease_correction['adjustment_to_operating_earnings'] + research_correction['adjustment_to_operating_income']
    total_debt_correction = lease_correction['debt_value_of_leases']
    
    return invested_capital_correction, ebit_correction, total_debt_correction

def overall_investment_stats(yearly_items, 
                        yearly_reinvestment,
                        research_correction,
                        lease_correction):
    
    ebit = yearly_items['operatingIncome']['ttm']
    invested_capital = yearly_reinvestment['invested_capital'][0]
    total_debt = yearly_items['shortLongTermDebtTotal']['ttm']
    
    
    invested_capital_correction, ebit_correction, total_debt_correction = get_corrected_lease_and_research(research_correction, 
                                                                                                           lease_correction,
                                                                                                           ebit, 
                                                                                                           invested_capital,
                                                                                                           total_debt)

    corrected_debt = total_debt + total_debt_correction
    corrected_ebit = yearly_items['operatingIncome']['ttm'] + ebit_correction
    corrected_invested_capital = (invested_capital_correction + yearly_reinvestment['invested_capital'][0])
    
    income_tax_expense = list(yearly_items['incomeTaxExpense'].values())
    if sum(income_tax_expense[:3]) < 1:
        income_tax_expense = list(yearly_items['taxProvision'].values())
        
    new_reinvestment_rate = yearly_reinvestment['reinvestment'][0] / (corrected_ebit - income_tax_expense[0])
    new_return_on_capital = (corrected_ebit - income_tax_expense[0]) / corrected_invested_capital
    
    return {
        'old_invested_capital': yearly_reinvestment['invested_capital'][0],
        'new_invested_capital': corrected_invested_capital,
        'old_reinvestment_rate': yearly_reinvestment['reinvestment_rate'][0],
        'new_reinvestment_rate': new_reinvestment_rate,
        'old_return_on_capital': yearly_reinvestment['return_on_capital'][0],
        'new_return_on_capital': new_return_on_capital,
        'old_ebit': yearly_items['operatingIncome']['ttm'],
        'corrected_ebit': corrected_ebit,
        'old_debt': total_debt,
        'corrected_debt': corrected_debt,
        'old_sales_to_capital': yearly_items['totalRevenue']['ttm'] / yearly_reinvestment['invested_capital'][0],
        'new_sales_to_capital': yearly_items['totalRevenue']['ttm'] / corrected_invested_capital,
    }

def dcf_calculate_inputs(revenues, 
                 corrected_operating_income, 
                 estimated_margins,
                 terminal_estimated_margin,
                 estimated_growth_rate, 
                 cost_of_capital,
                 terminal_cost_of_capital,
                 terminal_growth_rate,
                 effective_tax_rate,
                 marginal_tax_rate,
                 sales_to_capital_ratio,
                 decay = 5,
                 margin_convergence = 5, 
                 max_years = 10):
    
    temp_growth_rate = estimated_growth_rate
    temp_tax_rate = effective_tax_rate
    temp_discount_rate = cost_of_capital
    temp_margins = estimated_margins
    
    current_revenues = revenues    
    
    revenue_growth_rate_list = []
    revenues_list = []
    estimated_margins_list = []
    tax_rate_list = []
    cost_of_capital_list = []
    
    for i in range(1, max_years + 1):
        past_revenue = current_revenues
        temp_growth_rate = next_rate(i, decay, estimated_growth_rate, terminal_growth_rate)
        temp_tax_rate = next_tax(i, decay, temp_tax_rate, effective_tax_rate, marginal_tax_rate)
        temp_discount_rate = next_discount(i, decay, temp_discount_rate, cost_of_capital, terminal_cost_of_capital)
        temp_margins = next_margin(i, margin_convergence, estimated_margins, terminal_estimated_margin)
        
        current_revenues = ( 1 + temp_growth_rate) * current_revenues
        
        revenue_growth_rate_list.append(temp_growth_rate)
        revenues_list.append(current_revenues)
        estimated_margins_list.append(temp_margins)
        tax_rate_list.append(temp_tax_rate)
        cost_of_capital_list.append(temp_discount_rate)
    
    return {
        'revenue_growth_rate_list': revenue_growth_rate_list,
        'revenues_list': revenues_list,
        'estimated_margins_list': estimated_margins_list,
        'tax_rate_list': tax_rate_list,
        'cost_of_capital_list': cost_of_capital_list        
    }

def dcf_continued_calculations(base_revenue,
                        revenues_list,
                        estimated_margins_list,
                        tax_rate_list,
                        cost_of_capital_list,
                        sales_to_capital_ratio):
    
    fcff_list = []
    
    ebit_after_tax_list = [x * y * (1 - z) for x, y, z in zip(revenues_list, estimated_margins_list, tax_rate_list)]
    
    revenues_list = [base_revenue] + revenues_list
    reinvestment_list = [(revenues_list[i + 1] - revenues_list[i])/ sales_to_capital_ratio if sales_to_capital_ratio > 0 else 0 for i in range(len(revenues_list) - 1)]
    reinvestment_list = [max(x, 0) for x in reinvestment_list]
    
    fcff_list = [x - y for x, y in zip(ebit_after_tax_list, reinvestment_list)]
    
    cumulative_discount = 1
    
    cumulative_discount_list = []
    
    for discount_rate in cost_of_capital_list:
        cumulative_discount *= ( 1 / ( 1 + discount_rate))
        cumulative_discount_list.append(cumulative_discount)
        
    pv_fcff_list = [x * y for x, y in zip(cumulative_discount_list, fcff_list)]
    
    return {
        'ebit_after_tax_list': ebit_after_tax_list,
        'reinvestment_list': reinvestment_list,
        'fcff_list': fcff_list,
        'pv_fcff_list': pv_fcff_list,
        'cumulative_discount_list': cumulative_discount_list
    }

def dcf_final_calculations(revenue_growth_rate_list,
                           revenues_list,
                           estimated_margins_list,
                           tax_rate_list,
                           cost_of_capital_list,
                           ebit_after_tax_list,
                           reinvestment_list,
                           fcff_list,
                           pv_fcff_list,
                           cumulative_discount_list,
                           terminal_growth_rate,
                           debt,
                           cash,
                           no_of_shares,
                           minority_interest = 0,
                           non_operating_assets = 0,
                           prob_of_failure = 0,
                           proceeds_if_firm_fails = 0,
                           value_of_options = 0):
    
    terminal_discount = cost_of_capital_list[-1]
    terminal_revenue = revenues_list[-1] * (1 + terminal_growth_rate)
    terminal_margins = estimated_margins_list[-1]
    
    terminal_ebit = terminal_revenue * terminal_margins
    terminal_tax = tax_rate_list[-1]
    terminal_ebit_after_tax = terminal_ebit * (1 - terminal_tax)
    
    terminal_reinvestment = (terminal_growth_rate / terminal_discount) * terminal_ebit_after_tax if terminal_growth_rate > 0 else 0
    terminal_fcff = terminal_ebit_after_tax - terminal_reinvestment   
    
    terminal_value = terminal_fcff / (terminal_discount - terminal_growth_rate)
    pv_terminal_value = terminal_value * cumulative_discount_list[-1]
    pv_cash_flow = sum(pv_fcff_list)
    pv_sum = pv_terminal_value + pv_cash_flow
    
    value_of_operating_assets = pv_sum * ( 1 - prob_of_failure ) + proceeds_if_firm_fails * prob_of_failure
    value_of_equity = value_of_operating_assets - debt - minority_interest + cash + non_operating_assets
    value_of_equity_common_stock =  value_of_equity - value_of_options
    
    estimated_value_per_share = value_of_equity_common_stock / no_of_shares
    
    return {
        'terminal_growth_rate': terminal_growth_rate,
        'terminal_discount': terminal_discount,
        'terminal_revenue': terminal_revenue,
        'terminal_margins': terminal_margins,
        'terminal_tax': terminal_tax,
        'terminal_ebit': terminal_ebit,
        'terminal_ebit_after_tax': terminal_ebit_after_tax,
        'terminal_reinvestment': terminal_reinvestment,
        'terminal_fcff': terminal_fcff,
        'terminal_value': terminal_value,
        'pv_terminal_value': pv_terminal_value,
        'pv_cash_flow': pv_cash_flow,
        'pv_sum': pv_sum,
        'value_of_operating_assets': value_of_operating_assets,
        'value_of_equity': value_of_equity,
        'value_of_equity_common_stock': value_of_equity_common_stock,
        'estimated_value_per_share': estimated_value_per_share,
    }
    
def perform_dcf_from_base_inputs(revenues, 
                 corrected_operating_income, 
                 estimated_margins,
                 terminal_estimated_margin,
                 estimated_growth_rate, 
                 cost_of_capital,
                 terminal_cost_of_capital,
                 terminal_growth_rate,
                 effective_tax_rate,
                 marginal_tax_rate,
                 sales_to_capital_ratio,
                 debt,
                 cash,
                 no_of_shares,
                 decay = 5,
                 margin_convergence = 5, 
                 max_years = 10,
                 minority_interest = 0,
                 non_operating_assets = 0,
                 prob_of_failure = 0,
                 proceeds_if_firm_fails = 0,
                 value_of_options = 0,
                 debug = False):
    
    base_input_calculation = dcf_calculate_inputs(revenues, 
                 corrected_operating_income, 
                 estimated_margins,
                 terminal_estimated_margin,
                 estimated_growth_rate, 
                 cost_of_capital,
                 terminal_cost_of_capital,
                 terminal_growth_rate,
                 effective_tax_rate,
                 marginal_tax_rate,
                 sales_to_capital_ratio,
                 decay,
                 margin_convergence, 
                 max_years)
    
    intermediate_calculation = dcf_continued_calculations(revenues,
                                        base_input_calculation['revenues_list'],
                                        base_input_calculation['estimated_margins_list'],
                                        base_input_calculation['tax_rate_list'],
                                        base_input_calculation['cost_of_capital_list'],
                                        sales_to_capital_ratio)
    
    final_calc = dcf_final_calculations(base_input_calculation['revenue_growth_rate_list'],
                               base_input_calculation['revenues_list'],
                               base_input_calculation['estimated_margins_list'],
                               base_input_calculation['tax_rate_list'],
                               base_input_calculation['cost_of_capital_list'],
                               intermediate_calculation['ebit_after_tax_list'],
                               intermediate_calculation['reinvestment_list'],
                               intermediate_calculation['fcff_list'],
                               intermediate_calculation['pv_fcff_list'],
                               intermediate_calculation['cumulative_discount_list'],
                               terminal_growth_rate,                           
                               debt,
                               cash,
                               no_of_shares,
                               minority_interest,
                               non_operating_assets,
                               prob_of_failure,
                               proceeds_if_firm_fails,
                               value_of_options)
    if debug:
        print('revenue_growth_rate_list')
        print(base_input_calculation['revenue_growth_rate_list'])
        print()
        print('revenues_list')
        print(base_input_calculation['revenues_list'])
        print()
        print('estimated_margins_list')
        print(base_input_calculation['estimated_margins_list'])
        print()
        print('tax_rate_list')
        print(base_input_calculation['tax_rate_list'])
        print()
        print('cost_of_capital_list')
        print(base_input_calculation['cost_of_capital_list'])
        print()
        print('ebit_after_tax_list')
        print(intermediate_calculation['ebit_after_tax_list'])
        print()
        print('reinvestment_list')
        print(intermediate_calculation['reinvestment_list'])
        print()
        print('fcff_list')
        print(intermediate_calculation['fcff_list'])
        print()
        print('pv_fcff_list')
        print(intermediate_calculation['pv_fcff_list'])
        print()
        print('final_calc')
        print(final_calc)
        
    return {
        "revenue_growth_rate_list": base_input_calculation['revenue_growth_rate_list'],
        "revenues_list": base_input_calculation['revenues_list'],
        "estimated_margins_list": base_input_calculation['estimated_margins_list'],
        "tax_rate_list": base_input_calculation['tax_rate_list'],
        "cost_of_capital_list": base_input_calculation['cost_of_capital_list'],
        "ebit_after_tax_list": intermediate_calculation['ebit_after_tax_list'],
        "reinvestment_list": intermediate_calculation['reinvestment_list'],
        "fcff_list": intermediate_calculation['fcff_list'],
        "pv_fcff_list": intermediate_calculation['pv_fcff_list'],
        "final_calc": final_calc
    }
    
def perform_dcf_analysis(json_data, 
                         selected_growth_rate_option = 'mean_3y_est_growth_rate',
                         selected_sales_to_capital_option = 'mean_3y_sales_to_capital',
                         selected_operating_margin_option = 'mean_3y_margin',
                         operating_lease = 0,
                         terminal_margin = None,
                         terminal_risk_free_rate = None,
                         terminal_cost_of_capital = 'competitive',
                        custom_input = {},
                        debug = False):
    
    """
        terminal_cost_of_capital can be one of 'mature' or 'competitive'
    
    """
    assert(terminal_cost_of_capital in ['mature', 'competitive'])
    
    (
        industry,
        revenues_ttm,
        revenues_last_year,
        operating_income_ttm,
        operating_income_last_year,
        interest_expense_ttm,
        interest_expense_last_year,
        book_value_of_equity_ttm,
        book_value_of_equity_last_year,
        book_value_of_debt_ttm,
        book_value_of_debt_last_year,
        capital_lease,
        research_development,
        cash_ttm,
        cash_last_year,
        minority_interest_ttm,
        minority_interest_last_year,
        shares_outstanding,
        effective_tax_rate,
        marginal_tax_rate,
        market_cap,
        stock_price,
        reports_time_diff
    ) = convert_data_to_data_needed(json_data, debug = debug)
    
    if debug:
        print("Stock Data")
        print    ([
            industry,
            revenues_ttm,
            revenues_last_year,
            operating_income_ttm,
            operating_income_last_year,
            interest_expense_ttm,
            interest_expense_last_year,
            book_value_of_equity_ttm,
            book_value_of_equity_last_year,
            book_value_of_debt_ttm,
            book_value_of_debt_last_year,
            capital_lease,
            research_development,
            cash_ttm,
            cash_last_year,
            minority_interest_ttm,
            minority_interest_last_year,
            shares_outstanding,
            effective_tax_rate,
            marginal_tax_rate,
            market_cap,
            stock_price,
            reports_time_diff
        ])
    
    current_stock = json_data
    try:
        country = json_data['General']['AddressData']['Country']
    except:
        country = json_data['General']['CountryName']
        
    
    if industry['GicIndustry'] == None:
        sector = get_sector_convertor(code_data[f"{json_data['General']['Code']}.{json_data['General']['Exchange']}"]['industry'][0])
    else:
        sector = get_sector_convertor(industry['GicIndustry'])
    
    equity_risk_premium = get_latest_equity_risk_premium_cached()
    
    yearly_items = get_comparison_items(current_stock, ['totalRevenue', 
                                     'operatingIncome',
                                     'capitalLeaseObligations',
                                     'totalStockholderEquity',
                                     'shortLongTermDebtTotal',
                                     'cashAndShortTermInvestments',
                                     'cash',
                                     'interestExpense',
                                     'capitalExpenditures',
                                     'totalCurrentAssets',
                                     'totalCurrentLiabilities',
                                     'changeInWorkingCapital',
                                    'depreciationAndAmortization',
                                    'reconciledDepreciation',
                                     'incomeTaxExpense',
                                    'taxProvision',
                                     'researchDevelopment',                 
                                      'intangibleAssets',
                                     'goodWill',
                                     'totalAssets',
                                    'totalLiab'], max_years = 10)
    
    if debug:
        print('YEARLY ITEMS')
        print(yearly_items)
        print()
    
    historical_growth = get_yearly_differences(yearly_items, ['totalRevenue'])
    yearly_reinvestment = get_yearly_reinvestment(yearly_items)

    if debug:
        print('Yearly Reinvestment')
        print(yearly_reinvestment)
        print()
    
    bond_rates = get_bond_yield_latest(country)
    
    (cost_of_capital, 
     lease_correction, 
     risk_free_rate, 
     debt_to_equity_ratio, 
     interest_coverage_ratio, 
     calculations) = get_cost_of_capital(country,
                sector, 
                book_value_of_equity_ttm, 
                book_value_of_debt_ttm, 
                operating_income_ttm, 
                interest_expense_ttm,
                market_cap, 
                bond_rates, 
                equity_risk_premium,
                operating_lease,
                pct_of_revenues = 1,
                debug = debug )
    
    research_correction = research_development_conversion(list(yearly_items['researchDevelopment'].values()),
                                                          sector)

    correction_to_accounting_data = overall_investment_stats(yearly_items, 
                        yearly_reinvestment,
                        research_correction,
                        lease_correction)
    
    operating_income_ttm = correction_to_accounting_data['corrected_ebit']
    book_value_of_debt_ttm = correction_to_accounting_data['corrected_debt']
    invested_capital = correction_to_accounting_data['new_invested_capital']
    
    if len(yearly_reinvestment['est_growth_rate']) == 0:
        yearly_reinvestment['est_growth_rate'] = [historical_growth['totalRevenue']['cagr']]
        
    if (correction_to_accounting_data['new_reinvestment_rate'] > 0 
        and correction_to_accounting_data['new_return_on_capital'] > 0):
        latest_corrected_growth_rate = (correction_to_accounting_data['new_reinvestment_rate'] * 
                                      correction_to_accounting_data['new_return_on_capital'])
    else:
        latest_corrected_growth_rate = yearly_reinvestment['est_growth_rate'][0]
    
    latest_corrected_margin = operating_income_ttm / revenues_ttm if revenues_ttm > 0 else 0
     
    difference_of_growth_rates = yearly_reinvestment['est_growth_rate'][0] - latest_corrected_growth_rate 
    difference_of_sales_to_capital = yearly_reinvestment['sales_to_capital'][0] - correction_to_accounting_data['new_sales_to_capital']
    difference_of_margins = yearly_reinvestment['operating_margin'][0] - latest_corrected_margin
    
    possible_estimated_growth_rates = {
        'historical_cagr': historical_growth['totalRevenue']['cagr'],
        'mean_est_growth_rate': clamp_difference( statistics.mean(yearly_reinvestment['est_growth_rate']) , difference_of_growth_rates),
        'mean_3y_est_growth_rate': clamp_difference( statistics.mean(yearly_reinvestment['est_growth_rate'][:3]) , difference_of_growth_rates),
        'max_est_growth_rate': clamp_difference( max(yearly_reinvestment['est_growth_rate']) , difference_of_growth_rates),
        'latest_est_growth_rate': latest_corrected_growth_rate,
        'industry_projection': None,
        'industry_average': None,
        'industry_best': None
    }
    
    possible_sales_to_capital = {
        'latest_sales_to_capital': yearly_reinvestment['sales_to_capital'][-1],
        'mean_3y_sales_to_capital': clamp_difference( statistics.mean(yearly_reinvestment['sales_to_capital'][:3]) , difference_of_sales_to_capital),
        'mean_sales_to_capital': clamp_difference( statistics.mean(yearly_reinvestment['sales_to_capital']) , difference_of_sales_to_capital),
        'max_sales_to_capital': clamp_difference( max(yearly_reinvestment['sales_to_capital']) , difference_of_sales_to_capital),
        'corrected_latest_sales_to_capital': correction_to_accounting_data['new_sales_to_capital'],
        'industry_average': None,
        'industry_best': None
    }
    
    possible_margins = {
        'latest_margin': latest_corrected_margin,
        'mean_3y_margin': clamp_difference( statistics.mean(yearly_reinvestment['operating_margin'][:3]) , difference_of_margins),
        'mean_margin': clamp_difference( statistics.mean(yearly_reinvestment['operating_margin']) , difference_of_margins),
        'max_margin': clamp_difference( max(yearly_reinvestment['operating_margin']) , difference_of_margins),
        'industry_average': None,
        'industry_best': None
    }
    
    terminal_growth_rate = terminal_risk_free_rate if terminal_risk_free_rate else risk_free_rate
    
    estimated_growth_rate = possible_estimated_growth_rates[selected_growth_rate_option]
    
    estimated_margins = possible_margins[selected_operating_margin_option]
    
    terminal_estimated_margins = terminal_margin if terminal_margin else estimated_margins
    
    if terminal_cost_of_capital == 'mature':
        terminal_cost_of_capital = bond_rates + equity_risk_premium
    else:
        terminal_cost_of_capital = cost_of_capital
    
    sales_to_capital = possible_sales_to_capital[selected_sales_to_capital_option]
    
    if sales_to_capital < 0:
        print('Unable to use the sales to capital option, reverting to "corrected_latest_sales_to_capital"')
        sales_to_capital = possible_sales_to_capital['corrected_latest_sales_to_capital']
    
    revenues_ttm = custom_input.get('revenues_ttm', revenues_ttm)
    operating_income_ttm = custom_input.get('operating_income_ttm', operating_income_ttm)
    estimated_margins = custom_input.get('estimated_margins', estimated_margins)
    terminal_estimated_margins = custom_input.get('terminal_estimated_margins', terminal_estimated_margins)
    estimated_growth_rate = custom_input.get('estimated_growth_rate', estimated_growth_rate)
    cost_of_capital = custom_input.get('cost_of_capital', cost_of_capital)
    terminal_cost_of_capital = custom_input.get('terminal_cost_of_capital', terminal_cost_of_capital)
    terminal_growth_rate = custom_input.get('terminal_growth_rate', terminal_growth_rate)
    effective_tax_rate = custom_input.get('effective_tax_rate', effective_tax_rate)
    marginal_tax_rate = custom_input.get('marginal_tax_rate', marginal_tax_rate)
    sales_to_capital = custom_input.get('sales_to_capital', sales_to_capital)
    book_value_of_debt_ttm = custom_input.get('book_value_of_debt_ttm', book_value_of_debt_ttm)
    cash_ttm = custom_input.get('cash_ttm', cash_ttm)
    shares_outstanding = custom_input.get('shares_outstanding', shares_outstanding)

    if debug:
        print()
        print('Estimated Growth Rate', possible_estimated_growth_rates)
        print()
        print('Difference of growth rate', difference_of_growth_rates)
        print()
        print('Estimated Sales To Capital', possible_sales_to_capital)
        print()
        print('Difference of sales to capital', difference_of_sales_to_capital)
        print()
        print('Estimated Margins', possible_margins)
        print()
        print('Difference of margins', difference_of_margins)
        print()
        print('Correction To Accounting Data')
        print()
        print(correction_to_accounting_data)
        print()
        print('Inputs to DCF')
        print('revenues_ttm', revenues_ttm)
        print('operating_income_ttm', operating_income_ttm)
        print('estimated_margins', estimated_margins)
        print('terminal_estimated_margins', terminal_estimated_margins)
        print('estimated_growth_rate', estimated_growth_rate)
        print('cost_of_capital', cost_of_capital)
        print('terminal_cost_of_capital', terminal_cost_of_capital)
        print('terminal_growth_rate', terminal_growth_rate)
        print('effective_tax_rate', effective_tax_rate)
        print('marginal_tax_rate', marginal_tax_rate)
        print('sales_to_capital', sales_to_capital)
        print('book_value_of_debt_ttm', book_value_of_debt_ttm)
        print('cash_ttm', cash_ttm)
        print('shares_outstanding', shares_outstanding)
        print()
    
    dcf_results = perform_dcf_from_base_inputs( revenues_ttm, 
                 operating_income_ttm, 
                 estimated_margins,
                 terminal_estimated_margins,
                 estimated_growth_rate, 
                 cost_of_capital,
                 terminal_cost_of_capital,
                 terminal_growth_rate,
                 effective_tax_rate,
                 marginal_tax_rate,
                 sales_to_capital,
                 book_value_of_debt_ttm,
                 cash_ttm,
                 shares_outstanding
                 )  
    
    inputs_to_dcf = {
        'revenues_ttm': revenues_ttm,
        'operating_income_ttm': operating_income_ttm,
        'estimated_margins': estimated_margins,
        'terminal_estimated_margins': terminal_estimated_margins,
        'estimated_growth_rate': estimated_growth_rate,
        'cost_of_capital': cost_of_capital,
        'terminal_cost_of_capital': terminal_cost_of_capital,
        'terminal_growth_rate': terminal_growth_rate,
        'effective_tax_rate': effective_tax_rate,
        'marginal_tax_rate': marginal_tax_rate,
        'sales_to_capital': sales_to_capital,
        'book_value_of_debt_ttm': book_value_of_debt_ttm,
        'cash_ttm': cash_ttm,
        'shares_outstanding': shares_outstanding,
    }
    
    #yearly_items
    #yearly_reinvestment
    return {
        'dcf_results' : dcf_results,
        'inputs_to_dcf' : inputs_to_dcf,
        'possible_estimated_growth_rates': possible_estimated_growth_rates,
        'possible_sales_to_capital': possible_sales_to_capital,
        'possible_margins' : possible_margins,
        'yearly_reinvestment' : yearly_reinvestment,
        'yearly_items' : yearly_items,
        'calculations': calculations
    }


def get_cagr(metric_list, years=10):
    metric_list = metric_list[:years]
    if metric_list[0] > 0 and metric_list[-1] < 0:
        cagr = 99999
    elif metric_list[0] < 0 and metric_list[-1] < 0:
        cagr = 0
    elif metric_list[0] < 0 and metric_list[-1] > 0:
        cagr = -99999
    else:
        try:
            cagr = (metric_list[0]/metric_list[-1])**(1/(len(metric_list))) - 1
        except:
            cagr = 0
    return cagr

def calculate_fuzzy_score(current_stock, debug=False):
    last_year_date = list(current_stock['Financials']['Income_Statement']['yearly'].keys())[0]
    latest_quarter_date = list(current_stock['Financials']['Balance_Sheet']['quarterly'].keys())[0]
    
    (
        industry,
        revenues_ttm,
        revenues_last_year,
        operating_income_ttm,
        operating_income_last_year,
        interest_expense_ttm,
        interest_expense_last_year,
        book_value_of_equity_ttm,
        book_value_of_equity_last_year,
        book_value_of_debt_ttm,
        book_value_of_debt_last_year,
        capital_lease,
        research_development,
        cash_ttm,
        cash_last_year,
        minority_interest_ttm,
        minority_interest_last_year,
        shares_outstanding,
        effective_tax_rate,
        marginal_tax_rate,
        market_cap,
        stock_price,
        reports_time_diff
    ) = convert_data_to_data_needed(current_stock, debug = debug)

    yearly_items = get_comparison_items(current_stock, ['totalRevenue', 
                                     'operatingIncome',
                                     'netIncome::Income_Statement',
                                     'capitalLeaseObligations',
                                     'totalStockholderEquity',
                                     'shortLongTermDebtTotal',
                                     'cashAndShortTermInvestments',
                                     'cash',
                                     'interestExpense',
                                     'capitalExpenditures',
                                     'totalCurrentAssets',
                                     'totalCurrentLiabilities',
                                     'changeInWorkingCapital',
                                    'depreciationAndAmortization',
                                    'reconciledDepreciation',
                                     'incomeTaxExpense',
                                    'taxProvision',
                                     'researchDevelopment',
                                    'intangibleAssets',
                                     'goodWill',
                                     'totalAssets',
                                    'totalLiab',
                                    'netReceivables',
                                    'freeCashFlow',
                                     'propertyPlantEquipment',
                                      'costOfRevenue',
                                     'inventory'                
                                     ], max_years = 10)

    json_data = current_stock

    operating_lease = 0
    try:
        country = current_stock['General']['AddressData']['Country']
    except:
        country = current_stock['General']['CountryName']

    if industry['GicIndustry'] == None:
        sector = get_sector_convertor(code_data[f"{json_data['General']['Code']}.{json_data['General']['Exchange']}"]['industry'][0])
    else:
        sector = get_sector_convertor(industry['GicIndustry'])

    equity_risk_premium = get_latest_equity_risk_premium_cached()

    bond_rates = get_bond_yield_latest(country)

    (cost_of_capital, 
     lease_correction, 
     risk_free_rate, 
     debt_to_equity_ratio, 
     interest_coverage_ratio,
     calculations) = get_cost_of_capital(country,
                sector, 
                book_value_of_equity_ttm, 
                book_value_of_debt_ttm, 
                operating_income_ttm, 
                interest_expense_ttm,
                market_cap, 
                bond_rates, 
                equity_risk_premium,
                operating_lease,
                pct_of_revenues = 1,
                debug = debug)

    historical_growth_5_years = get_yearly_differences(yearly_items, ['totalRevenue', 'netIncome'], total_years = 5)
    historical_growth_3_years = get_yearly_differences(yearly_items, ['totalRevenue', 'netIncome'], total_years = 3)

    ttm_data = get_last_ttm(current_stock, ['totalRevenue', 'netIncome::Income_Statement'], years=2)

    reports_time_diff = (parser.parse(latest_quarter_date) - parser.parse(last_year_date)).days/365

    revenue_values = list(yearly_items['totalRevenue'].values())
    revenue_growth_5_years = historical_growth_5_years['totalRevenue']['cagr']
    revenue_growth_3_years = historical_growth_3_years['totalRevenue']['cagr']

    revenue_growth_ttm = (ttm_data['year 1']['totalRevenue'] - ttm_data['year 2']['totalRevenue']) / ttm_data['year 2']['totalRevenue'] if abs(ttm_data['year 2']['totalRevenue']) > 0 else 0

    earning_values = list(yearly_items['netIncome'].values())
    earnings_growth_5_years = historical_growth_5_years['netIncome']['cagr']
    earnings_growth_3_years = historical_growth_3_years['netIncome']['cagr']

    earnings_growth_ttm = (ttm_data['year 1']['netIncome'] - ttm_data['year 2']['netIncome']) / ttm_data['year 2']['netIncome'] if abs(ttm_data['year 2']['totalRevenue']) > 0 else 0

    yearly_reinvestment = get_yearly_reinvestment(yearly_items)

    latest_return_on_capital = yearly_reinvestment['return_on_capital'][0]
    mean_3y_return_on_capital = statistics.mean(yearly_reinvestment['return_on_capital'][:3])

    latest_est_growth_rate = yearly_reinvestment['est_growth_rate'][0]
    mean_3y_est_growth_rate = statistics.mean(yearly_reinvestment['est_growth_rate'][:3])

    
    operating_margin_values = [x for x in yearly_reinvestment['operating_margin'][:5] if x > 0]
    operating_margin_growth_5y = ((operating_margin_values[0]
                                   /operating_margin_values[-1])
                                  **(1/(len(operating_margin_values)))
                                  - 1) if len(operating_margin_values) > 1 else 0

    latest_operating_margin = yearly_reinvestment['operating_margin'][0]
    mean_3y_operating_margin = statistics.mean(yearly_reinvestment['operating_margin'][:3])

    quick_ratio_list =  [(x + y) / z if z > 1 else 0.0001 for x, y, z in zip(list(yearly_items['cashAndShortTermInvestments'].values()),
                                                         list(yearly_items['netReceivables'].values()),
                                                         list(yearly_items['totalCurrentLiabilities'].values())) ]

    current_ratio_list = [(x / y) if y > 1 else 0.0001 for x, y in zip(list(yearly_items['totalCurrentAssets'].values()),
                                                         list(yearly_items['totalCurrentLiabilities'].values())) ]

    roa_list = [(x / y) if y > 1 else 0.0001 for x, y in zip(earning_values,
                                                         list(yearly_items['totalAssets'].values())) ]

    roe_list = [(x / y) if y > 1 else 0.0001 for x, y in zip(earning_values,
                                                         list(yearly_items['totalStockholderEquity'].values())) ]
    roa_ttm = roa_list[0]
    roe_ttm = roe_list[0]

    quick_ratio_ttm = quick_ratio_list[0]
    current_ratio_ttm = current_ratio_list[0]

    roa_data = get_list_differences(roa_list,  total_years = 5)
    roe_data = get_list_differences(roe_list,  total_years = 5)

    roa_consistency_5y = roa_data['stdev']*100 #ideally less than 5
    roe_consistency_5y = roe_data['stdev']*100 #ideally less than 5

    free_cash_flow_to_revenue_list = [(w + x + y) / z if z > 1 else 0.0001 for w, x, y, z in zip(list(yearly_items['freeCashFlow'].values()),
                                                         list(yearly_items['capitalExpenditures'].values()),
                                                         list(yearly_items['propertyPlantEquipment'].values()),
                                                         list(yearly_items['totalRevenue'].values())) ]

    inventory_turnover_ratio_list = [(x / y) if y > 1 and x > 0 else 0.0001 for x, y in zip(list(yearly_items['costOfRevenue'].values()),
                                                         list(yearly_items['inventory'].values())) ]

    accrual_ratio_list =  [(x - y) / z if z > 1 else 0.0001 for x, y, z in zip(list(yearly_items['netIncome'].values()),
                                                         list(yearly_items['freeCashFlow'].values()),
                                                         list(yearly_items['totalAssets'].values())) ]

    free_cash_flow_to_revenue_ttm = free_cash_flow_to_revenue_list[0]
    inventory_turnover_ratio_ttm = inventory_turnover_ratio_list[0]
    accrual_ratio_ttm = accrual_ratio_list[0]
    book_value = yearly_reinvestment['book_value']
    book_value_growth_5y = get_cagr(book_value, years = 5)

    cost_and_return_capital_difference = latest_return_on_capital - cost_of_capital

    fundamentals_late  = {
        'book_value_growth_5y' :  book_value_growth_5y,
        'accrual_ratio_ttm' :  accrual_ratio_ttm,
        'inventory_turnover_ratio_ttm'  : inventory_turnover_ratio_ttm,
        'free_cash_flow_to_revenue_ttm' :  free_cash_flow_to_revenue_ttm,
        'roa_consistency_5y' :  roa_consistency_5y,
        'roe_consistency_5y' :  roe_consistency_5y,
        'roa_ttm' :  roa_ttm,
        'roe_ttm' :  roe_ttm,
        'current_ratio_ttm' :  current_ratio_ttm,
        'quick_ratio_ttm' :  quick_ratio_ttm,
        'mean_3y_operating_margin'  : mean_3y_operating_margin,
        'operating_margin_growth_5y' :  operating_margin_growth_5y,
        'mean_3y_est_growth_rate' :  mean_3y_est_growth_rate,
        'latest_return_on_capital' :  latest_return_on_capital,
        'earnings_growth_5_years'  : earnings_growth_5_years,
        'revenue_growth_5_years' :  revenue_growth_5_years,
        'debt_to_equity_ratio' :  debt_to_equity_ratio,
        'interest_coverage_ratio' :  interest_coverage_ratio,
        'cost_and_return_capital_difference' :  cost_and_return_capital_difference
    }

    fundamentals_medium = {
        'latest_est_growth_rate'  : latest_est_growth_rate,
        'latest_return_on_capital' :  latest_return_on_capital,
        'roa_ttm' :  roa_ttm,
        'roe_ttm' :  roe_ttm,
        'earnings_growth_ttm' :  earnings_growth_ttm,
        'revenue_growth_ttm' :  revenue_growth_ttm,
        'latest_operating_margin'  : latest_operating_margin,
        'cost_and_return_capital_difference'  : cost_and_return_capital_difference,
        'inventory_turnover_ratio_ttm'  : inventory_turnover_ratio_ttm,
        'free_cash_flow_to_revenue_ttm' :  free_cash_flow_to_revenue_ttm,
        'accrual_ratio_ttm' :  accrual_ratio_ttm,
        'current_ratio_ttm' :  current_ratio_ttm,
        'quick_ratio_ttm' :  quick_ratio_ttm
    }

    fuzzy_schema_late = stockapi.read_json("/root/stock/stock/notebook/eod_data/fuzzy_schema_late.json")
    fuzzy_schema_medium = stockapi.read_json("/root/stock/stock/notebook/eod_data/fuzzy_schema_medium.json")

    assert(set(fuzzy_schema_late.keys()) == set(fundamentals_late.keys()))
    assert(set(fuzzy_schema_medium.keys()) == set(fundamentals_medium.keys()))
    
    for k, v in fundamentals_late.items():
        if isinstance(v, complex):
            print(k, v)
            
    medium_fuzzy = stockapi.calculate_fundamental_score(fundamentals_medium, fuzzy_schema_medium, fuzzy_score_threshold = 0.8)
    late_fuzzy = stockapi.calculate_fundamental_score(fundamentals_late, fuzzy_schema_late, fuzzy_score_threshold = 0.8)
    
    final_metrics = {
            'book_value_growth_5y' :  book_value_growth_5y,
            'accrual_ratio_ttm' :  accrual_ratio_ttm,
            'inventory_turnover_ratio_ttm'  : inventory_turnover_ratio_ttm,
            'free_cash_flow_to_revenue_ttm' :  free_cash_flow_to_revenue_ttm,
            'roa_consistency_5y' :  roa_consistency_5y,
            'roe_consistency_5y' :  roe_consistency_5y,
            'roa_ttm' :  roa_ttm,
            'roe_ttm' :  roe_ttm,
            'current_ratio_ttm' :  current_ratio_ttm,
            'quick_ratio_ttm' :  quick_ratio_ttm,
            'mean_3y_operating_margin'  : mean_3y_operating_margin,
            'operating_margin_growth_5y' :  operating_margin_growth_5y,
            'mean_3y_est_growth_rate' :  mean_3y_est_growth_rate,
            'latest_return_on_capital' :  latest_return_on_capital,
            'earnings_growth_5_years'  : earnings_growth_5_years,
            'revenue_growth_5_years' :  revenue_growth_5_years,
            'debt_to_equity_ratio' :  debt_to_equity_ratio,
            'interest_coverage_ratio' :  interest_coverage_ratio,
            'cost_and_return_capital_difference' :  cost_and_return_capital_difference,
            'latest_est_growth_rate'  : latest_est_growth_rate,
            'latest_operating_margin'  : latest_operating_margin,
            'cost_of_capital': cost_of_capital,
            'earnings_growth_ttm' :  earnings_growth_ttm,
            'revenue_growth_ttm' :  revenue_growth_ttm,
            'sector': sector,
            'stock_price': stock_price,
            'company_name': current_stock['General']['Name'],
            'shares_outstanding': shares_outstanding
        }

    return({
        'medium_fuzzy': medium_fuzzy,
        'late_fuzzy': late_fuzzy,
        'metrics': final_metrics
    })