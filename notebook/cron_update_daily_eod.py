from new_stock_api import (
    update_bulk_stock_price,
    update_bond_price,
    API_TOKEN,
    get_bond_yield_latest,
    get_latest_stock_price,
    calculate_fuzzy_score,
    perform_dcf_analysis
)

from mongodb import MetricsDB
import stockapi
from datetime import datetime
import os

if __name__ == "__main__":
    update_bulk_stock_price(API_TOKEN, 'KLSE')
    update_bond_price(API_TOKEN)
    failed_codes = []
    base_dir = '/root/stock/stock/notebook/eod_data/klse_stock_fundamentals/'
    stock_list = os.listdir(base_dir)
    metrics_db = MetricsDB()
    for stock in sorted(stock_list):
        try:
            current_stock = stockapi.read_json(f'{base_dir}/{stock}')
            stock_fundamentals = calculate_fuzzy_score(current_stock, debug=False)
            dcf = perform_dcf_analysis(current_stock, debug = False)
            metrics_db.insert([{
                'stock_code': stock.split('.')[0],
                'exchange': 'KLSE',
                'dcf': dcf,
                'metrics': stock_fundamentals,
                'date': datetime.today().isoformat()
            }])
        except Exception as e:
            failed_codes.append(stock)
            print(e)

    stockapi.write_json('/root/stock/stock/notebook/eod_data/failed_codes', datetime.today().isoformat(), failed_codes, compress=False)