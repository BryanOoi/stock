def upside_score(upside):
    if upside > 0 and upside < 0.3:
        return 0.5
    elif upside > 0.3 and upside < 0.5:
        return 0.8
    elif upside > 0.5:
        return 1
    else:
        return 0
    
    
def recommendation_rating(recommendation):
    if recommendation > 0.5:
        return 'Strong Buy'
    elif recommendation > 0.3 and recommendation < 0.5:
        return 'Buy'
    elif recommendation > 0.1 and recommendation < 0.3:
        return 'Hold'
    else:
        return 'Sell'
    

from new_stock_api import (
    update_bulk_stock_price,
    update_bond_price,
    API_TOKEN,
    get_bond_yield_latest,
    get_latest_stock_price,
    calculate_fuzzy_score,
    perform_dcf_analysis
)
import warnings
warnings.filterwarnings('ignore')

from mongodb import MetricsDB, FundamentalDataDB
import stockapi
from datetime import datetime
import os
from mongodb import FundamentalDataDB

if __name__ == "__main__":

    MARKET = 'KLSE'
    fundamental_db = FundamentalDataDB()
    fundamental_items = fundamental_db.query({'General.Exchange': MARKET}, {'General.Exchange': 1, 'General.Code': 1})
    
    update_bulk_stock_price(API_TOKEN, 'KLSE')
    update_bond_price(API_TOKEN)
    failed_codes = []
    metrics_db = MetricsDB()
    for stock in fundamental_items:
        try:
            current_stock = fundamental_db.query({'General.Exchange': MARKET, 'General.Code': stock['General']['Code']})[0]
            stock_fundamentals = calculate_fuzzy_score(current_stock, debug=False)
            dcf = perform_dcf_analysis(current_stock, debug = False)
            try:
                certainty = max(1 - abs((dcf['possible_estimated_growth_rates']['mean_3y_est_growth_rate'] - 
                                         stock_fundamentals['metrics']['revenue_growth_5_years'])
                                        /dcf['possible_estimated_growth_rates']['mean_3y_est_growth_rate']), 
                                0.2)
            except:
                certainty = 0        
            upside = round((dcf['dcf_results']['final_calc']['estimated_value_per_share'] - stock_fundamentals['metrics']['stock_price']) / stock_fundamentals['metrics']['stock_price'], 2)

            recommendation = certainty * upside_score(upside) * stock_fundamentals['late_fuzzy']['percent_fuzzy_score']

            rating = recommendation_rating(recommendation)

            recommendation_data = {
                "certainty": certainty,
                "upside": upside,
                "recommendation": recommendation,
                "rating": rating,
            }

            metrics_db.insert([{
                'stock_code': stock['General']['Code'],
                'exchange': MARKET,
                'dcf': dcf,
                'metrics': stock_fundamentals,
                'recommendation_data': recommendation_data,
                'date': datetime.today().isoformat()
            }])
            print(stock_fundamentals['metrics']['company_name'])
        except Exception as e:
            failed_codes.append(stock)
            print(e)


    stockapi.write_json('/root/stock/stock/stockapp/app/dcf/notebook/eod_data/failed_codes', datetime.today().isoformat(), failed_codes, compress=False)