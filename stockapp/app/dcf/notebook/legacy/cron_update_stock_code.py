import stockapi
import json
import pandas as pd
from datetime import timedelta
import random

if __name__ == "__main__":
    cnx = stockapi.connect_db()
    df = stockapi.read_data(cnx)
    df = df.sort_values("datetime_mined", ascending=True)
    df.drop_duplicates(subset ="stockcode", inplace = True, keep="last") 
    df = df.sort_values("datetime_mined", ascending=False)

    df = df[df['datetime_mined'] > df.iloc[0]['datetime_mined'] - timedelta(days=7)]
    
    new_stock_code_list = list(df['stockcode'])
    
    random.shuffle(new_stock_code_list)
    
    stockapi.write_json("/root/stock/stock/stockapp/app/dcf/notebook/", "stock_code_list.json.gz", new_stock_code_list)