import stockapi
import json
import pandas as pd
from datetime import timedelta
import random

if __name__ == "__main__":
    new_stock_code_list = stockapi.read_json("/root/stock/stock/stockapp/app/dcf/notebook/stock_code_list.json.gz")
    
    working_stock_list = new_stock_code_list
    
    for i in range(7):
        res = stockapi.retrieve_reuter_fundamentals(stock_code_list=working_stock_list)

        failed_codes = res['failed_codes']
        print(len(failed_codes))
        if len(failed_codes) == 0:
            break
        working_stock_list = failed_codes
        time.sleep(20)
