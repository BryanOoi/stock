from new_stock_api import (
    get_stock_eod,
    API_TOKEN
)
from mongodb import FundamentalDataDB
import requests
import stockapi
import time
from datetime import datetime
import warnings
warnings.filterwarnings('ignore')

if __name__ == "__main__":
    EXCHANGE_CODE = 'NASDAQ'
    x = requests.get(f'https://eodhistoricaldata.com/api/exchange-symbol-list/{EXCHANGE_CODE}?api_token={API_TOKEN}&fmt=json') 
    
    code_list = [x['Code'] for x in x.json()]
    date = datetime.today().isoformat()
    fundamental_db = FundamentalDataDB()
    for code in code_list:
        try:
            fundamental_db = FundamentalDataDB()
            x = requests.get(f'https://eodhistoricaldata.com/api/fundamentals/{code}.US?api_token={API_TOKEN}')
            current_stock = x.json()
            identifier = f"{current_stock['General']['Code']}.{current_stock['General']['Exchange']}"
            current_stock['stock_code_full'] = identifier 
            current_stock['last_updated'] = date 
            fundamental_db.insert([current_stock])        
            time.sleep(0.1)
        except Exception as e:
            print (e)
