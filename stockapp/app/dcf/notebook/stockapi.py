import json
import gzip
import os
import datetime
import re 
import requests
import datetime
from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, DateTime, Float
import pandas as pd
import time
import random
import pprint
from scipy import stats
import numpy as np

url = "http://www.bursamarketplace.com/index.php"
url_reuters =  "https://www.reuters.com/companies/api"
FINANCIALS_LOC = "/root/stock/stock/stockapp/app/dcf/notebook/stock_financials/MY"

def write_json(dir, filename, data, compress = True):
    """
    Write json files
    Args:
        dir: str, directory to write to
        filename: str, filename
        data: python object
        compress: boolean, default: True, files compress to gzip
    Returns:
        None
    """
    if '.json' not in filename:
        filename += '.json'
    if '.gz' not in filename and compress:
        filename += '.gz'
    filepath = os.path.join(dir, filename)

    if not os.path.isdir(dir):
        os.makedirs(dir)

    if compress:
        with gzip.GzipFile(filepath, 'w') as f:
            f.write(json.dumps(data).encode('utf-8'))
    else:
        with open(filepath, 'w') as f:
            json.dump(data, f)
    print('Created file : {}'.format(filepath))


def read_json(filepath):
    """
    Read json files
    Args:
        filename: str, full/relative file path to json file, or gziped json file
    Returns:
        python object
    """
    if filepath.split('.')[-1] == 'gz':
        with gzip.GzipFile(filepath, 'r') as f:
            data = json.loads(f.read().decode('utf-8'))
    else:
        with open(filepath) as f:
            data = json.load(f)

    return data
    

stock_code_list = []

try:
    stock_code_list = read_json("/root/stock/stock/stockapp/app/dcf/notebook/stock_code_list.json.gz")
except:
    pass


def get_stock_fundamentals(data_obj, data_obj_financials, current_share_price):
    
    """ 
      hongleong_financials = read_json(".\\financial_statements_hongleong.json.gz")
      hongleong_key_metrics = read_json(".\\key_metrics_hongleong.json.gz")     
      data_obj = hongleong_key_metrics["market_data"]
      data_obj_financials = hongleong_financials["market_data"]      
    """
    
    if not current_share_price:
        current_share_price = data_obj["last"]
        
    annual_income_data = data_obj_financials["financial_statements"]["income"]["annual"]
    
    interest_coverage_ratio = -99999.0
    if "Total Interest Expense" in annual_income_data:
        interest_coverage_ratio = float(data_obj["ebitd_annual"])/float(annual_income_data["Total Interest Expense"][0]["value"])
    
#     price_to_book = -99999.0
#     if float(data_obj["book_value_share_quarterly"]) > 0.05:
#         price_to_book = float(current_share_price) / float(data_obj["book_value_share_quarterly"])
    
    roa_consistency = -99999.0
    if float(data_obj["roaa_5y"]) > 0.05:
        roa_consistency = float(data_obj["roa_rfy"])/float(data_obj["roaa_5y"])
        
    roe_consistency = -99999.0
    if float(data_obj["roae_5y"]) > 0.05:
        roe_consistency = float(data_obj["roe_ttm"])/float(data_obj["roae_5y"])
        
    free_cash_flow_to_revenue = -99999.0
    if float(data_obj["revenue_ttm"]) > 0.05:
        free_cash_flow_to_revenue = (float(data_obj["fcf_ttm"]) / float(data_obj["revenue_ttm"]))     
        
    return {
        "five_years_earning_growth_rate" : float(data_obj["eps_growth_5y"]),
        "roi" : float(data_obj["roi_annual"]),
        "debt_to_equity" : float(data_obj["lt_debt_equity_quarterly"]),
        "quick_ratio" : float(data_obj["quick_ratio_quarterly"]),
        "payout_ratio" : float(data_obj["payout_ratio_annual"]),
        "current_ratio" : float(data_obj["current_ratio_annual"]),
#         "price_to_book" : price_to_book,
        "roa_annual" : float(data_obj["roa_rfy"]),
        "roe_ttm" : float(data_obj["roe_ttm"]),
        "roa_consistency" : roa_consistency,
        "roe_consistency" : roe_consistency,
        "free_cash_flow_to_revenue" : free_cash_flow_to_revenue,
        "book_value_growth" : float(data_obj["book_value_share_growth_5y"]),
        "interest_coverage_ratio" : interest_coverage_ratio,
        "inventory_turnover_ratio" : float(data_obj["inventory_turnover_annual"]),
        "net_margin_growth_5y" : float(data_obj["net_margin_growth_5y"]),
        "net_income_trendline" : trendline(annual_income_data['Net Income'])
    }

def calculate_fundamental_score(fundamentals, schema, fuzzy_score_threshold = 0.8):
    fuzzy_score = 0
    absolute_score = 0
    percent_absolute_score = 0
    
    total_metrics_counted = 0
    
    item_passed_fuzzy_score = []
    item_failed_fuzzy_score = []
    metric_counted = []
    metric_not_counted = []
    
    for key, values in fundamentals.items():
        if values > -99999.0:
            total_metrics_counted += 1
            calc_schema = schema[key]
            fundamental_val = fundamentals[key]
            metric_counted.append([key, fundamental_val])
            calc_eq = schema[key]['calc_eq']
            weight = schema[key]['weight']
            absolute_pass = eval(calc_eq.replace("x", str(fundamental_val)))
            all_digits = re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?", calc_eq)
            fuzzy_obj = {
                            "type": key,
                            "value": fundamental_val,
                            "threshold_values": all_digits,
                            "fuzzy_score": 1,
                            "calc_eq" : calc_eq
                        }
            
            if absolute_pass:
                absolute_score += 1
                fuzzy_score += 1 * weight
                item_passed_fuzzy_score.append(fuzzy_obj)
            else:
#                 all_digits = [float(x) for x in all_digits]
                fuzzy_scores = [1 - (abs(float(x)-fundamental_val)/float(x))  for x in all_digits]
                
                highest_fuzzy_score = max(fuzzy_scores)
                if highest_fuzzy_score < 0:
                    highest_fuzzy_score = 0
                highest_fuzzy_score *=  weight
                fuzzy_score += highest_fuzzy_score 
                fuzzy_obj["fuzzy_score"] = highest_fuzzy_score
                
                if highest_fuzzy_score >= fuzzy_score_threshold:                 
                    item_passed_fuzzy_score.append(fuzzy_obj)
                else:
                    item_failed_fuzzy_score.append(fuzzy_obj)
        else:
            metric_not_counted.append(key)
                
    return {
        "absolute_score": absolute_score,
        "fuzzy_score": fuzzy_score,
        "percent_absolute_score": absolute_score/total_metrics_counted,
        "percent_fuzzy_score": fuzzy_score/total_metrics_counted,
        "total_metric_counted": total_metrics_counted,
        "item_passed_fuzzy_score" : item_passed_fuzzy_score,
        "item_failed_fuzzy_score" : item_failed_fuzzy_score,
        "metric_counted": metric_counted,
        "metric_not_counted" : metric_not_counted
    }
#                 for digit in all_digits:
#                     fuzzy_score-cand = 1 - (abs(digit-fundamental_val)/digit)
#                     print(percent_diff)

def get_time():
    return datetime.datetime.now().replace(microsecond=0).isoformat()
    
def get_bursa_meta_data():
    query_string = "?screentype=stocks&board=all&tpl=screener_ajax&type=getResult&action=listing&pagenum=1&sfield=srscoreavg&stype=asc"

    x = requests.get(url + query_string)
    records = x.json()
    del records["records"]
    return records

def get_bursa_stock_data(page_num):
    time_now = get_time()
    query_string = "?screentype=stocks&board=all&tpl=screener_ajax&type=getResult&action=listing&pagenum=" + str(page_num) + "&sfield=srscoreavg&stype=asc"

    x = requests.get(url + query_string)
    records = x.json()["records"]
    
    #bursa data has a weird quirk where items more than page 2 has the following format (temp = {'10': {'cashtag': '$VSTE', ....)
    if page_num != 1:
        records = [x for _, x in records.items()]
    for item in records:
        item["datetime_mined"] = time_now
    
    return records

def get_detailed_stock_data(code, only_analyst_consensus=False):
    clean_code = code.split(".")[0]
    query_strings = {"financial_details" : "?tpl=stock_ajax&type=gettixdetail&code=" + clean_code, 
                     "analyst_consensus" : "?tpl=recommend_ajax&type=stockanalystconsensis&code=" + code,
                     "news" : "?tpl=news_ajax&type=listing&pagenum=1&markettype=stock&stockcode=" + clean_code}
    
    if only_analyst_consensus:
        del query_strings["financial_details"]
        del query_strings["news"]
    results = {}
    
    for query_type, query_string in query_strings.items():
        x = requests.get(url + query_string)
        time.sleep(get_rand_sec())
    
        records = x.json()
        results[query_type] = records
    
    return results


def connect_db():
    POSTGRES_ADDRESS = 'localhost' ## INSERT YOUR DB ADDRESS IF IT'S NOT ON PANOPLY
    POSTGRES_PORT = '5442'
    POSTGRES_USERNAME = 'deploy' ## CHANGE THIS TO YOUR PANOPLY/POSTGRES USERNAME
    POSTGRES_PASSWORD = 'docker' ## CHANGE THIS TO YOUR PANOPLY/POSTGRES PASSWORD 
    POSTGRES_DBNAME = 'stocks' ## CHANGE THIS TO YOUR DATABASE NAME

    postgres_str = ('postgresql://{username}:{password}@{ipaddress}:{port}/{dbname}'
                    .format(username=POSTGRES_USERNAME,
                    password=POSTGRES_PASSWORD,
                    ipaddress=POSTGRES_ADDRESS,
                    port=POSTGRES_PORT,
                    dbname=POSTGRES_DBNAME))
    # Create the connection
    cnx = create_engine(postgres_str, pool_size=10,
                                      max_overflow=2,
                                      pool_recycle=300,
                                      pool_pre_ping=True,
                                      pool_use_lifo=True)
    
    return cnx


def get_table(cnx, table_name="bursa_screener"):
    meta = MetaData()
    meta.reflect(bind=cnx)
    return meta.tables[table_name]


def insert_data(cnx, data, table):
    cnx.execute(table.insert(),data)

def read_data(cnx, table_name="bursa_screener"):
    return pd.read_sql_query('SELECT * FROM ' + table_name + ';', cnx)

def get_rand_sec(l=10, t=20):
    return random.randrange(l,t)

def retrieve_daily_data(start_pg=1):
    cnx = connect_db()
    bursa_screener_table = get_table(cnx)
    meta_data = get_bursa_meta_data()
    total_pages = meta_data['totalpage']
    print("Completed retrieving metadata")
    time.sleep(get_rand_sec())
    for i in range(start_pg, total_pages + 1):
        records = get_bursa_stock_data(i)
        print("Completed retrieving stocks on page " + str(i) )
        insert_data(cnx, records, bursa_screener_table)
        time.sleep(get_rand_sec())
    print("Successfully retrieved info for all " + str(total_pages) + " pages.")

def get_reuters_info(code):
    query_string = "/getFetchCompanyFinancials/" + code
    x = requests.get(url_reuters + query_string)
    time.sleep(get_rand_sec(l=15, t=20))

    records = x.json()
    
    return records

def get_reuters_key_metrics(code):
    query_string = "/getFetchCompanyKeyMetrics/" + code
    x = requests.get(url_reuters + query_string)
    time.sleep(get_rand_sec(l=15, t=20))

    records = x.json()
    
    return records
    
def get_reuters_profile(code):
    query_string = "/getFetchCompanyProfile/" + code
    x = requests.get(url_reuters + query_string)
    time.sleep(get_rand_sec(l=15, t=20))

    records = x.json()
    
    return records
    
def get_info_retry(data_func, stock_code, retry_max = 3):
    retry_count = 0
    res = None
    data_obj = data_func(stock_code)
    while data_obj["status"]["code"] != 200:
        data_obj = data_func(stock_code)
        retry_count += 1
        if retry_count >= retry_max:
            return None
    return data_obj
    
def get_stock_code_by_prices(df, prices):
    val = df["price"].isin(prices)
    df2 = df[
        val
    ]
    return list(df2["stockcode"].to_dict().values())

def get_stock_details(df, stocks):
    val = df["stockcode"].isin(stocks)
    return df[val]


def retrieve_reuter_fundamentals(starting_stock=None, stock_code_list=stock_code_list):
    cnx = connect_db()
    fundamental_score_tbl = get_table(cnx, table_name="fundamental_score")
    
    fuzzy_schema = read_json("/root/stock/stock/stockapp/app/dcf/notebook/fuzzy_schema.json")
    
    stock_to_fundamentals = {}
    failed_codes = []
    total_code_count = 0
    
    if starting_stock in stock_code_list:
        stock_code_list = stock_code_list[stock_code_list.index(starting_stock):]
    
    random.shuffle(stock_code_list) 
    for stock_code in stock_code_list:
        try:
            financials = get_info_retry(get_reuters_info, stock_code)
            key_metrics = get_info_retry(get_reuters_key_metrics, stock_code)
            if financials != None:
                write_json(FINANCIALS_LOC, stock_code, financials)
                
            if financials == None or key_metrics == None:
                failed_codes.append(stock_code)
                print("Error found for {}".format(stock_code))
                continue 
            total_code_count += 1
            fundamentals = get_stock_fundamentals(key_metrics["market_data"], financials["market_data"], None)
            fundamental_score = calculate_fundamental_score(fundamentals, fuzzy_schema)
            time_now = get_time()
#             db_data = {
#                 "stockcode": stock_code,
#                 "absolute_score": fundamental_score["absolute_score"],
#                 "fuzzy_score": fundamental_score["fuzzy_score"],
#                 "percent_absolute_score": fundamental_score["percent_absolute_score"],
#                 "percent_fuzzy_score": fundamental_score["percent_fuzzy_score"],
#                 "total_metric_counted": fundamental_score["total_metric_counted"],
#                 "key_metrics": json.dumps(key_metrics),
#                 "financials":  json.dumps(financials),
#                 "fundamental_obj": json.dumps(fundamentals),
#                 "fundamental_score_obj": json.dumps(fundamental_score),
#                 "datetime_mined": time_now
#             }
            # reduce load on DB
            db_data = {
                "stockcode": stock_code,
                "absolute_score": fundamental_score["absolute_score"],
                "fuzzy_score": fundamental_score["fuzzy_score"],
                "percent_absolute_score": fundamental_score["percent_absolute_score"],
                "percent_fuzzy_score": fundamental_score["percent_fuzzy_score"],
                "total_metric_counted": fundamental_score["total_metric_counted"],
                "key_metrics": json.dumps(key_metrics),
                "financials":  json.dumps({}),
                "fundamental_obj": json.dumps(fundamentals),
                "fundamental_score_obj": json.dumps({}),
                "datetime_mined": time_now
            }
            stock_to_fundamentals["stock_code"] = db_data
            print("For stock {}, absolute_score == {}, fuzzy_score == {}, percent_absolute_score == {}, percent_fuzzy_score == {}".format(
                        stock_code, 
                        str(round(fundamental_score["absolute_score"], 3)), 
                        str(round(fundamental_score["fuzzy_score"], 3)),
                        str(round(fundamental_score["percent_absolute_score"], 3)),   
                        str(round(fundamental_score["percent_fuzzy_score"], 3)),  
                    )
                 )
            insert_data(cnx, db_data, fundamental_score_tbl)
        except Exception as e:
            print(e)
            print("Error found for {}".format(stock_code))
            failed_codes.append(stock_code)
    
    time_now = get_time()
    
    return {
        "failed_codes": failed_codes
    }

def retrieve_company_info(starting_stock=None, stock_code_list=stock_code_list):
    cnx = connect_db()
    company_profile_table = get_table(cnx, table_name="company_profile")
    
    failed_codes = []
    
    if starting_stock in stock_code_list:
        stock_code_list = stock_code_list[stock_code_list.index(starting_stock):]
        
    for stock_code in stock_code_list:
        try:
            company_info = get_info_retry(get_reuters_profile, stock_code)
            
#             detailed_bursa_data = get_detailed_stock_data(stock_code, True)
            detailed_bursa_data = {}
            time_now = get_time()
            db_data = company_info["market_data"]
            pt_avg = 0
            try:
                pt_avg = float(detailed_bursa_data["analyst_consensus"]["mean"].replace("RM", ""))
            except:
                print("Cannot retrieve pt_avg for {}".format(stock_code))
            db_data["pt_avg"] = pt_avg
#             db_data["pt_date"] = detailed_bursa_data["analyst_consensus"]["recommendationdate"]
#             db_data["pt_status"] = detailed_bursa_data["analyst_consensus"]["recommendationstatus"]
#             db_data["pt_gainloss"] = detailed_bursa_data["analyst_consensus"]["potentialgainloss"]
#             db_data["pt_difference"] = detailed_bursa_data["analyst_consensus"]["difference"]
            db_data["pt_date"] = 0
            db_data["pt_status"] = 0
            db_data["pt_gainloss"] = 0
            db_data["pt_difference"] = 0
            db_data["datetime_mined"] = time_now
            db_data["sig_devs"] = json.dumps(db_data["sig_devs"])
            db_data["officers"] = json.dumps(db_data["officers"])
            db_data["street_address"] = json.dumps(db_data["street_address"])
            db_data["phone"] = json.dumps(db_data["phone"])
            db_data["stockcode"] = stock_code
            
            print("Retrieved company info for {}".format(stock_code))

            insert_data(cnx, db_data, company_profile_table)
        except Exception as e:
            print(e)
            print("Error found for {}".format(stock_code))
            failed_codes.append(stock_code)
    time_now = get_time()
    
    return failed_codes
    
def get_fundamental_dict(cnx, stock_code):
    df_fundamental = read_data(cnx, "fundamental_score")
    df_single_res = df_fundamental[df_fundamental["stockcode"] == stock_code]
    return df_single_res.to_dict()
    
def show_fundamental_score(cnx, stock_code):
    stock_fundamental = get_fundamental_dict(cnx, stock_code)
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(json.loads(list(stock_fundamental["fundamental_score_obj"].values())[0]))
    
def trendline(data, order=1):
    data = [float(x['value']) for x in data]
    data.reverse()
    data = stats.zscore(data)

    index = list(range(len(data)))
    coeffs = np.polyfit(index, list(data), order)
    slope = coeffs[-2]
    return round(float(slope) * 100, 2)