import os
import pymongo
from pymongo import UpdateOne, DeleteOne

MONGO_USERNAME = os.environ.get('MONGO_USERNAME', False)
MONGO_PASSWORD = os.environ.get('MONGO_PASSWORD', False)

ROOT_PATH = os.environ.get('ROOT_PATH', 'app/dcf')
if not ROOT_PATH:
    ROOT_PATH = 'app/dcf'

if ROOT_PATH == 'app/dcf':
    MONGO_URL = 'mongodb_container'
else:
    MONGO_URL = 'localhost'

MONGO_CONNECTION_STRING = f"mongodb://{MONGO_USERNAME}:{MONGO_PASSWORD}@{MONGO_URL}:27017/admin?authSource=admin&compressors=disabled&gssapiServiceName=mongodb"

def get_mongo_obj(mongo_url = MONGO_CONNECTION_STRING):
    myclient = pymongo.MongoClient(MONGO_CONNECTION_STRING)

    return myclient


class MongoDB:
    def __init__(self, mongo_url = MONGO_CONNECTION_STRING, database='local', table='metrics_data'):
        self.myclient = get_mongo_obj(mongo_url)
        self.mydb = self.myclient[database]
        self.mycol = self.mydb[table]
        
    def insert(self, mylist):
        """
        Insert documents
        Args:
            mylist : List of objects E.g. [ { "name": "Amy", "address": "Apple st 652" } ]
        Returns:
            ids of inserted objects
        """
        x = self.mycol.insert_many(mylist)
        self.last_inserted_ids = x.inserted_ids
        return self.last_inserted_ids
    
    def query(self, myquery, projection = None):
        """
        Queries documents
        Args:
            myquery : Query object E.g. { "address": {"$regex": "^S"} }
        Returns:
            Query results
        """
        if projection:
            mydoc = self.mycol.find(myquery, projection)
        else:
            mydoc = self.mycol.find(myquery)
        return [x for x in mydoc]

    def update_one(self, myquery, set_obj):
        """
        Queries documents
        Args:
            myquery : Query object E.g. { "address": {"$regex": "^S"} }
            set_obj: Set query object to E.g.    {
                    $set: { "size.uom": "cm", status: "P" },
                    $currentDate: { lastModified: true }
                }
        Returns:
            True
        """
        mydoc = self.mycol.update_one(myquery, set_obj)
        return True

    def update_many(self, myquery, set_obj):
        """
        Queries documents
        Args:
            myquery : Query object E.g. { "address": {"$regex": "^S"} }
            set_obj: Set query object to E.g.    {
                    $set: { "size.uom": "cm", status: "P" },
                    $currentDate: { lastModified: true }
                }
        Returns:
            True
        """
        mydoc = self.mycol.update_many(myquery, set_obj)
        return True
  
    def delete(self,myquery,confirm=True):
        """
        Deletes documents
        Args:
            myquery : Query object E.g. { "address": {"$regex": "^S"} }
        Returns:
            Count of deleted objects
        """
        msg = 'WARNING: Proceed to delete ? (Yes/No)'
        if confirm:
            ans = input(msg)
            if not ans.lower() in ['y', 'yes', 'yup', 'yea']:
                print('INFO : Skip Delete')

        print('INFO : Proceeding with Delete ')
        x = self.mycol.delete_many(myquery)
        print(x.deleted_count, " documents deleted.")
        return x.deleted_count
        
class MetricsDB(MongoDB):
    def __init__(self):
        super().__init__(table='metrics_data')

    def insert(self, mylist):
        operations = []
        for item in mylist:
            key = {'stock_code':item['stock_code']}
            data = item
            operations.append(UpdateOne(key, {"$set": data},upsert=True))
        self.mycol.bulk_write(operations)
        return True

    def delete_many(self, mylist):
        operations = []
        for item in mylist:
            operations.append(DeleteOne(item))
        self.mycol.bulk_write(operations)
        return True        
       
        
class StockPriceDB(MongoDB):
    def __init__(self):
        super().__init__(table='stock_price')

    def insert(self, mylist):
        operations = []
        for item in mylist:
            key = {'code':item['code']}
            data = item
            operations.append(UpdateOne(key, {"$set": data},upsert=True))
        self.mycol.bulk_write(operations)
        return True

    def delete_many(self, mylist):
        operations = []
        for item in mylist:
            operations.append(DeleteOne(item))
        self.mycol.bulk_write(operations)
        return True      
    
    
class BondPriceDB(MongoDB):
    def __init__(self):
        super().__init__(table='bond_price')

    def insert(self, mylist):
        operations = []
        for item in mylist:
            key = {'country':item['country']}
            data = item
            operations.append(UpdateOne(key, {"$set": data},upsert=True))
        self.mycol.bulk_write(operations)
        return True

    def delete_many(self, mylist):
        operations = []
        for item in mylist:
            operations.append(DeleteOne(item))
        self.mycol.bulk_write(operations)
        return True    
        
class EquityPremiumDB(MongoDB):
    def __init__(self):
        super().__init__(table='equity_risk_premium')

    def insert(self, mylist):
        operations = []
        for item in mylist:
            key = {'type':item['type']}
            data = item
            operations.append(UpdateOne(key, {"$set": data},upsert=True))
        self.mycol.bulk_write(operations)
        return True

    def delete_many(self, mylist):
        operations = []
        for item in mylist:
            operations.append(DeleteOne(item))
        self.mycol.bulk_write(operations)
        return True    
    
class FundamentalDataDB(MongoDB):
    def __init__(self):
        super().__init__(table='fundamental_data')

    def insert(self, mylist):
        operations = []
        for item in mylist:
            key = {'stock_code_full':item['stock_code_full']}
            data = item
            operations.append(UpdateOne(key, {"$set": data},upsert=True))
        self.mycol.bulk_write(operations)
        return True

    def delete_many(self, mylist):
        operations = []
        for item in mylist:
            operations.append(DeleteOne(item))
        self.mycol.bulk_write(operations)
        return True    
    
    
class HistoricalStockPriceDB(MongoDB):
    def __init__(self):
        super().__init__(table='historical_stock_price')

    def insert(self, mylist):
        operations = []
        for item in mylist:
            key = {'stock_code_full':item['stock_code_full']}
            data = item
            operations.append(UpdateOne(key, {"$set": data},upsert=True))
        self.mycol.bulk_write(operations)
        return True

    def delete_many(self, mylist):
        operations = []
        for item in mylist:
            operations.append(DeleteOne(item))
        self.mycol.bulk_write(operations)
        return True      