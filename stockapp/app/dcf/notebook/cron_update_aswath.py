from new_stock_api import (
    total_urls,
    get_latest_website_date,
    refresh_aswath_excel_data,
    get_latest_equity_risk_premium
)
from mongodb import EquityPremiumDB
from datetime import datetime

if __name__ == "__main__":
    latest_website_date = get_latest_website_date()
    refresh_aswath_excel_data(latest_website_date, total_urls)
    equity_premium = EquityPremiumDB()
    latest_premium = get_latest_equity_risk_premium()
    equity_premium.insert([
        {
            'type': 'implied',
            'premium': latest_premium,
            'date': datetime.today().isoformat()
        }
    ])