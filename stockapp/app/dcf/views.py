# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.http import JsonResponse, HttpResponse
from django.template import loader

import json

from app.dcf.notebook.mongodb import MetricsDB, FundamentalDataDB
from app.dcf.notebook.new_stock_api import perform_dcf_analysis, generate_pagination_query
import requests
import os

header_converter = {
    'revenue_growth_rate_list': 'Revenue growth rate',
    'revenues_list': 'Revenues',
    'estimated_margins_list': 'EBIT (Operating} margin',
    'tax_rate_list': 'Tax rate',
    'cost_of_capital_list': 'Cost of capital',
    'ebit_after_tax_list': 'EBIT (1-t)',
    'reinvestment_list': '- Reinvestment',
    'fcff_list': 'FCFF',
    'pv_fcff_list': 'PV(FCFF)'
}

header_full_data_converter = {
    'adjusted_ebit': 'Adjusted Ebit',
    'invested_capital': 'Invested Capital',
    'reinvestment': 'Reinvestment',
    'reinvestment_rate': 'Reinvestment Rate',
    'reinvestment_rate_2': 'Reinvestment Rate (invested capital)',
    'return_on_capital': 'Return on capital',
    'operating_margin': 'Operating Margin',
    'sales_to_capital': 'Sales to capital',
    'est_growth_rate': 'Est. Growth Rate',
    'est_growth_rate_2': 'Est. Growth Rate (invested capital)',
    'book_value': 'Book value',
}

header_to_terminal = {
    'revenue_growth_rate_list': 'terminal_growth_rate',
    'revenues_list': 'terminal_revenue',
    'estimated_margins_list': 'terminal_margins',
    'tax_rate_list': 'terminal_tax',
    'cost_of_capital_list': 'terminal_discount',
    'ebit_after_tax_list': 'terminal_ebit_after_tax',
    'reinvestment_list': 'terminal_reinvestment',
    'fcff_list': 'terminal_fcff'
}

@login_required(login_url="/login/")
def dcf_view(request, stock_code, exchange):
    return render(request, "dcf.html", {"stock_code": stock_code, "exchange": exchange})


@login_required(login_url="/login/")
def full_data_view(request, stock_code, exchange):
    return render(request, "full_data.html", {"stock_code": stock_code, "exchange": exchange})


@login_required(login_url="/login/")
def stock_list_view(request, exchange):
    return render(request, "stock_list.html", {"exchange": exchange})


def get_dcf_data_from_stock(stock_code, exchange, custom_inputs=None):
    metrics_db = MetricsDB()

    all_metrics = None
    if custom_inputs:
        for k, v in custom_inputs.items():
            custom_inputs[k] = float(v)
        # API_TOKEN = os.environ['API_TOKEN']
        # x = requests.get(
        #     f'https://eodhistoricaldata.com/api/fundamentals/{stock_code}.{exchange}?api_token={API_TOKEN}')
        fundamental_db = FundamentalDataDB()
        json_data = fundamental_db.query({'General.Exchange': exchange, 'General.Code': stock_code})[0]
        dcf_calc = perform_dcf_analysis(json_data, custom_input=custom_inputs)
        temp_metrics = metrics_db.query(
            {'exchange': exchange, 'stock_code': stock_code},
            {'stock_code': 1,
             'exchange': 1,
             'dcf': 1,
             'metrics': 1,
             'date': 1})

        if len(temp_metrics) == 0:
            return False

        temp_metrics = temp_metrics[0]

        all_metrics = {'dcf': dcf_calc, 'metrics': temp_metrics['metrics']}
    else:
        all_metrics = metrics_db.query(
            {'exchange': exchange, 'stock_code': stock_code},
            {'stock_code': 1,
             'exchange': 1,
             'dcf': 1,
             'metrics': 1,
             'date': 1})

        if len(all_metrics) == 0:
            return False

        all_metrics = all_metrics[0]

    stock_data = all_metrics['dcf']['dcf_results']

    inputs_to_dcf = all_metrics['dcf']['inputs_to_dcf']

    total_data = []
    for i, (k, v) in enumerate(stock_data.items()):
        if k != 'final_calc':
            temp_data = {'header_name': header_converter.get(k, k),
                         'id': i + 1}
            for j, val in enumerate(v):
                temp_data[f'year{j+1}'] = round(val, 2)
            terminal_val = stock_data['final_calc'].get(
                header_to_terminal.get(k, ''), '-')
            if terminal_val != '-':
                terminal_val = round(float(terminal_val), 2)
            temp_data['terminal'] = terminal_val
            total_data.append(temp_data)

    final_calc = stock_data['final_calc']
    final_calc['id'] = 1
    inputs_to_dcf['id'] = 1

    return {
        'dcf_data': total_data,
        'final_calc': [final_calc],
        'inputs_to_dcf': inputs_to_dcf,
        'metrics': all_metrics['metrics']
    }


def get_yearly_reinvestment(stock_code, exchange):
    metrics_db = MetricsDB()

    all_metrics = metrics_db.query(
        {'exchange': exchange, 'stock_code': stock_code})

    if len(all_metrics) == 0:
        return False

    all_metrics = all_metrics[0]

    stock_metrics = all_metrics['metrics']['metrics']
    medium_fuzzy = all_metrics['metrics']['medium_fuzzy']['percent_fuzzy_score']
    late_fuzzy = all_metrics['metrics']['late_fuzzy']['percent_fuzzy_score']

    yearly_items = all_metrics['dcf']['yearly_items']

    yearly_items_list = []
    for i, (k, v) in enumerate(yearly_items.items()):
        current_row = {'header_name': k}
        current_row.update(v)
        current_row['id'] = i + 1
        yearly_items_list.append(current_row)

    yearly_reinvestment = all_metrics['dcf']['yearly_reinvestment']

    estimated_growth_rate = all_metrics['dcf']['possible_estimated_growth_rates']

    estimated_sales_to_capital = all_metrics['dcf']['possible_sales_to_capital']

    estimated_margins = all_metrics['dcf']['possible_margins']

    correction_to_accounting_data = all_metrics['dcf'].get(
        'correction_to_accounting_data', {})

    estimated_growth_rate['id'] = 1
    estimated_sales_to_capital['id'] = 1
    estimated_margins['id'] = 1
    correction_to_accounting_data['id'] = 1
    stock_metrics['id'] = 1

    calculations = all_metrics['dcf']['calculations']

    total_data = []
    for i, (k, v) in enumerate(yearly_reinvestment.items()):
        if k != 'final_calc':
            temp_data = {'header_name': header_full_data_converter.get(k, k),
                         'id': i + 1}
            for j, val in enumerate(v):
                if j == 0:
                    temp_data[f'ttm'] = round(val, 2)
                else:
                    temp_data[f'year{j}'] = round(val, 2)

            total_data.append(temp_data)

    return {
        'yearly_items': yearly_items_list,
        'yearly_reinvestment': total_data,
        'estimated_growth_rate': [estimated_growth_rate],
        'estimated_sales_to_capital': [estimated_sales_to_capital],
        'estimated_margins': [estimated_margins],
        'correction_to_accounting_data': [correction_to_accounting_data],
        'calculations': calculations,
        'stock_metrics': [stock_metrics],
        'medium_fuzzy': medium_fuzzy,
        'late_fuzzy': late_fuzzy
    }


@login_required(login_url="/login/")
def retrieve_dcf_data(request, stock_code, exchange):

    data = {}
    msg = None
    success = False

    if request.method == "POST":
        data = get_dcf_data_from_stock(stock_code, exchange)
        print(data)
        if data:
            success = True
        else:
            msg = f"ERROR: Unable to retrieve data for the stock code {stock_code}"

        return JsonResponse({"data": data, "msg": msg, "success": success})

    elif request.method == "GET":
        return JsonResponse({"msg": 'Request failed'})


@login_required(login_url="/login/")
def submit_custom_dcf(request, stock_code, exchange):

    data = {}
    msg = None
    success = False

    if request.method == "POST":
        custom_inputs = request.body.decode('utf-8')
        custom_inputs = json.loads(custom_inputs)
        data = get_dcf_data_from_stock(stock_code, exchange, custom_inputs)
        if data:
            success = True
        else:
            msg = f"ERROR: Unable to retrieve data for the stock code {stock_code}"

        return JsonResponse({"data": data, "msg": msg, "success": success})

    elif request.method == "GET":
        return JsonResponse({"msg": 'Request failed'})


@login_required(login_url="/login/")
def retrieve_yearly_reinvestment(request, stock_code, exchange):

    data = {}
    msg = None
    success = False

    if request.method == "POST":
        data = get_yearly_reinvestment(stock_code, exchange)
        if data:
            success = True
        else:
            msg = f"ERROR: Unable to retrieve data for the stock code {stock_code}"

        return JsonResponse({"data": data, "msg": msg, "success": success})

    elif request.method == "GET":
        return JsonResponse({"msg": 'Request failed'})


@login_required(login_url="/login/")
def get_stock_list(request, exchange):

    data = {}
    msg = None
    success = False

    if request.method == "POST":
        post_data = request.body.decode('utf-8')
        post_data = json.loads(post_data)
        next_key = post_data.get('next_key', None)

        metrics_db = MetricsDB()
        query = {}
        filter_fields = {
            'metrics.medium_fuzzy.percent_fuzzy_score': 1,
            'metrics.late_fuzzy.percent_fuzzy_score': 1,
            'metrics.metrics': 1,
            'recommendation_data': 1,
            'stock_code': 1,
            'dcf.dcf_results.final_calc.estimated_value_per_share': 1}

        sort = ['metrics.late_fuzzy.percent_fuzzy_score', -1]
        limit = 100
        query, next_key_fn = generate_pagination_query(query, sort, next_key)

        stock_list = list(metrics_db.mycol.find(
            query, filter_fields).limit(limit).sort([sort]))
        stock_list_formatted = []
        for stock in stock_list:
            temp = stock['metrics']['metrics']
            temp['medium_fuzzy'] = stock['metrics']['medium_fuzzy']['percent_fuzzy_score']
            temp['late_fuzzy'] = stock['metrics']['late_fuzzy']['percent_fuzzy_score']
            temp['stock_code'] = stock['stock_code']
            temp['estimated_valuation'] = stock['dcf']['dcf_results']['final_calc']['estimated_value_per_share']
            stock_list_formatted.append(temp)

        next_key = next_key_fn(stock_list)
        next_key['_id'] = str(next_key['_id'])

        if data:
            success = True
        else:
            msg = f"ERROR: Unable to retrieve stock list"

        return JsonResponse({"stock_list": stock_list_formatted, "next_key": next_key, "msg": msg, "success": success})

    elif request.method == "GET":
        return JsonResponse({"msg": 'Request failed'})
