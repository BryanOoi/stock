# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path
from app.dcf import views

urlpatterns = [
    path('dcf/<str:stock_code>/<str:exchange>/retrieve_dcf_data/', views.retrieve_dcf_data, name="retrieve_dcf_data"),
    path('dcf/<str:stock_code>/<str:exchange>/submit_custom_dcf/', views.submit_custom_dcf, name="submit_custom_dcf"),
    path('dcf/<str:stock_code>/<str:exchange>/full_data/retrieve_yearly_reinvestment/', views.retrieve_yearly_reinvestment, name="retrieve_yearly_reinvestment"),
    path('dcf/<str:stock_code>/<str:exchange>/', views.dcf_view, name="dcf"),
    path('dcf/<str:stock_code>/<str:exchange>/full_data/', views.full_data_view, name="full_data"),
    path('stock_list/<str:exchange>/', views.stock_list_view, name="stock_list_view"),
    path('stock_list/<str:exchange>/get_stock_list/', views.get_stock_list, name="get_stock_list"),
]
